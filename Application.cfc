<cfcomponent>
	
  <!--- settings///////////////////////////////////////--->
  <cfset this.name = "coffeeAuction" />
  <cfset this.clientManagement = 1 />
  <cfset this.mails = deserializeJSON(fileRead("/conf/" & (cgi.http_host contains "virtualcoffeeauctions.com" ? "prod" : "dev") & "/mailServers.json")) />

  <!--- public methods/////////////////////////////////--->

  <!--- function: onApplicationStart---------------------->
  <cffunction name="onApplicationStart">
    <cfset application.companyName = "Virtual Coffee Auctions" />
    <cfset application.productionDomain = "virtualcoffeeauctions.com" />
    <cfset application.isProductionEnvironment = cgi.http_host contains application.productionDomain />

    	<!--- load variables from config file --->
	<cfset structAppend(application, deserializeJSON(fileRead("/conf/" & (application.isProductionEnvironment ? "prod" : "dev") & "/conf.json")), 1) />

	<!--- load payment methods --->
	<cfquery name="application.qPaymentMethods" datasource="coffee_auction">
		SELECT * FROM paymentMethods
	</cfquery>

    <!--- cache controllers --->
    <cfset application.userController = new controllers.userController(
      new com.dao.UserDAO(application.datasource),
      new com.dao.CountryDAO(application.datasource),
      new com.dao.StatesDAO(application.datasource)
    ) />
    <cfset application.auctionController = new controllers.auctionController(
      new com.dao.AuctionDAO(application.datasource),
      new com.dao.MerchantDAO(application.datasource),
      new com.dao.UserDAO(application.datasource),
      new com.dao.BidDAO(application.datasource),
      application.bidRaiseMin
    ) />

  </cffunction>
  
  <!--- function: onRequestStart-------------------------->
  <cffunction name="onRequestStart">
	<cfparam type="string" name="url.restartKey" default="" />
	<cfparam type="string" name="request.message" default="" />
	<cfparam type="string" name="client.requestMessage" default="" />
	<cfparam type="string" name="client.user" default="#serializeJSON(new com.users.User())#" />

	<!--- check for restart key --->
	<cfif url.restartKey eq application.restartKey>
		<cfset onApplicationStart()>
	</cfif>
        
	<!--- set request variables --->
	<cfset request.section = listLast(getDirectoryFromPath(getBaseTemplatePath()), "/\")>
	<cfset request.isLandingPage = listContainsNoCase('wwwroot', request.section)>
	<cfset request.isAjaxRequest = request.section eq "ajax">
	<cfset request.user = new com.users.User(argumentCollection = deserializeJSON(client.user)) />

    <!--- if user is not logged in.. --->
    <cfif !request.user.getID() && listContains("account", request.section)>
      <cflocation url="/login" addtoken="0" />
    </cfif>

    <!--- do not include header if it is an ajax request --->
    <cfif !request.isAjaxRequest>
  		<cfinclude template="/includes/header.cfm">
    </cfif>
    
    <!--- relay any application messages --->
    <cfif len(client.requestMessage)>
      <cfset request.message = client.requestMessage>
      <cfset client.requestMessage = "">
    </cfif>
    
  </cffunction>

  <!--- function: onRequestEnd---------------------------->
  <cffunction name="onRequestEnd">
  	<cfif !request.isAjaxRequest>
  		<cfinclude template="/includes/footer.cfm">
    </cfif>
  </cffunction>
  
</cfcomponent>
