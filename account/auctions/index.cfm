<!---get data --->
<cfset stData = application.auctionController.viewMyAuctions(request.user.getID()) />

<!--- header --->
<div class="row">
	<div class="col-sm-12">
		<h1>My Auctions</h1>
	</div>
</div>

<!--- start content --->
<section class="row" id="myAuctions">
	<div class="col-sm">

		<!--- auctions that have been won but are unpaid --->
		<cfif arrayLen(stData.auctionsWonUnpaid)>
		<h2>Payment Pending</h2>
		<table class="table">
			<thead>
				<tr>
					<th scope="col">Auction ID</th>
					<th scope="col">Auction Started</th>
					<th scope="col">Auction Ended</th>
					<th scope="col">Actions</th>
				</tr>
			</thead>
			<tbody>
			<cfoutput>
				<cfloop array="#stData.auctionsWonUnpaid#" index="i" item="it">
				<tr>
					<td>#it.getID()#</td>
					<td>#dateFormat(it.getStartDate(), "short")#</td>
					<td>#dateFormat(it.getEndDate(), "short")#</td>
					<td><a href="/auctions/payment/?id=#it.getID()#" class="btn btn-primary"><i class="fas fa-dollar-sign"></i> Make payment</a></td>
				</tr>
				</cfloop>
			</cfoutput>
			</tbody>
		</table>
		</cfif>

		<!--- auctions active i am bidding on--->
		<cfif arrayLen(stData.auctionsParticipatedStillActive)>
		<h2>My Ongoing Auctions</h2>
		<table class="table">
			<thead>
				<tr>
					<th scope="col">Auction ID</th>
					<th scope="col">Auction Started</th>
					<th scope="col">Auction Ends</th>
					<th scope="col">Actions</th>
				</tr>
			</thead>
			<tbody>
			<cfoutput>
				<cfloop array="#stData.auctionsParticipatedStillActive#" index="i" item="it">
				<tr>
					<td>#it.getID()#</td>
					<td>#dateFormat(it.getStartDate(), "short")#</td>
					<td>#dateFormat(it.getEndDate(), "short")#</td>
					<td><a href="/auctions/details/?id=#it.getID()#" class="btn btn-primary"><i class="fas fa-eye"></i> View Details</a></td>
				</tr>
				</cfloop>
			</cfoutput>
			</tbody>
		</table>
		</cfif>

		<!--- auctions that have been won but are unpaid --->
		<cfif arrayLen(stData.auctionsWonPaid)>
		<h2>Auctions Previously Won</h2>
		<table class="table">
			<thead>
				<tr>
					<th scope="col">Auction ID</th>
					<th scope="col">Auction Started</th>
					<th scope="col">Auction Ended</th>
					<th scope="col">Actions</th>
				</tr>
			</thead>
			<tbody>
			<cfoutput>
				<cfloop array="#stData.auctionsWonPaid#" index="i" item="it">
				<tr>
					<td>#it.getID()#</td>
					<td>#dateFormat(it.getStartDate(), "short")#</td>
					<td>#dateFormat(it.getEndDate(), "short")#</td>
					<td><a href="/auctions/details/?id=#it.getID()#" target="_blank" class="btn btn-primary"><i class="fas fa-eye"></i> View Details</a></td>
				</tr>
				</cfloop>
			</cfoutput>
			</tbody>
		</table>
		</cfif>

	</div>
</section>
<!--- end content --->
