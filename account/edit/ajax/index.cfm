<!--- get data --->
<cfparam type="numeric" name="form.userID" default="0" />
<cfparam type="string" name="form.username" default="" />
<cfparam type="string" name="form.email" default="" />
<cfparam type="numeric" name="form.countryID" default="0" />
<cfparam type="string" name="form.password" default="" />
<cfparam type="string" name="form.confirmPassword" default="" />
<cfparam type="numeric" name="form.adminID" default="0" />
<cfparam type="string" name="form.state" default="" />
<cfparam type="string" name="form.otherState" default="" />
<cfparam type="string" name="form.shippingAddress_line1" default="" />
<cfparam type="string" name="form.shippingAddress_line2" default="" />

<!--- start try --->
<cftry>

  <cfset application.userController.updateUser(argumentCollection = form) />
  <cfset application.userController.reloadSessionUser(form.userID) />

  <!--- start error handling --->
  <cfcatch type="any">
    <cfheader statuscode="501" />
    <cfheader name="x-error-message" value="#cfcatch.message#" />
    <cfheader name="x-error-detail" value="#cfcatch.detail#" />
  </cfcatch>

</cftry>
<!--- end try --->