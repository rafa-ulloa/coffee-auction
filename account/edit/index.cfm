<!--- get data --->
<cfset stData = application.userController.viewUser(request.user.getID()) />
<cfset oUser = stData.user />

<!--- header --->
<header class="row">
<div class="col-sm clearfix">
<h1 class="float-left"><cfset writeOutput(oUser.getID() ? "Edit" : "Create") /> Account</h1>
    <a class="btn btn-primary float-right" href="/account" role="button"><i class="fas fa-arrow-left"></i> Back</a>
</div>
</header>

<!--- start form --->
<cfoutput>
    <section class="row justify-content-center">
    <div class="col-sm-12 col-md-6">

        <!--- start form --->
        <form id="form_user">

        <!--- hidden fields --->
        <input type="hidden" name="userID" value="#oUser.getID()#">
        <input type="hidden" name="adminID" value="1">

        <!--- username --->
        <div class="form-group row">
            <label for="form_user-input_username" class="col-sm-3 col-form-label">Username</label>
            <div class="col-sm-9">
                <input type="text" #oUser.getID() ? "readonly" : ""# class="form-control#oUser.getID() ? '-plaintext' : ''#" id="form_user-input_username" name="username" placeholder="Username">
            </div>
        </div>

        <!--- email --->
        <div class="form-group row">
            <label for="form_user-input_email" class="col-sm-3 col-form-label">Email</label>
            <div class="col-sm-9">
                <input type="email" class="form-control" id="form_user-input_email" name="email" placeholder="Email" value="#oUser.getEmail()#">
            </div>
        </div>

        <!--- country --->
        <div class="form-group row">
            <label for="form_user-input_country" class="col-sm-3 col-form-label">Country</label>
            <div class="col-sm-9">
                <select class="form-control" id="form_user-input_country" name="countryID">
                    <cfloop array="#stData.countries#" item="it">
                    <option value="#it.getID()#">#it.getNiceName()#</option>
                    </cfloop>
                </select>
            </div>
        </div>

        <!--- state --->
        <div class="form-group row">
            <label for="form_user-input_state" class="col-sm-3 col-form-label">State</label>
            <div class="col-sm-9">
                <input type="text" class="form-control form-state-field" id="form_user-input_otherState" name="otherState" placeholder="State/Province" />
                <select class="form-control form-state-field" id="form_user-input_state" name="state"></select>
            </div>
        </div>

        <!--- address line 1 --->
        <div class="form-group row">
            <label for="form_user-input_shippingAddress_line1" class="col-sm-3 col-form-label">Address Line 1</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="form_user-input_shippingAddress_line1" name="shippingAddress_line1" placeholder="Street Address" />
            </div>
        </div>

        <!--- address line 2 --->
        <div class="form-group row">
            <label for="form_user-input_shippingAddress_line2" class="col-sm-3 col-form-label">Address Line 2</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="form_user-input_shippingAddress_line2" name="shippingAddress_line2" placeholder="Apartment or Suite Number (optional)" />
            </div>
        </div>

    <!--- only show password fields for new users --->
    <cfif !oUser.getID()>

        <!--- password --->
        <div class="form-group row">
            <label for="form_user-input_password" class="col-sm-3 col-form-label">Password</label>
            <div class="col-sm-9">
                <input type="password" class="form-control" id="form_user-input_password" name="password" placeholder="Password" />
            </div>
        </div>

        <!--- confirm password --->
        <div class="form-group row">
            <label for="form_user-input_password-confirm" class="col-sm-3 col-form-label">Confirm Password</label>
            <div class="col-sm-9">
                <input type="password" class="form-control" id="form_user-input_password-confirm" name="confirmPassword" placeholder="Confirm password" />
            </div>
        </div>

    </cfif>
    <!--- end if --->

        <!--- submit --->
        <div class="form-group row">
        <div class="col-sm-12 text-center">
            <button type="submit" class="btn btn-primary" name="submit" value="submit"><i class="fas fa-check"></i> Submit</button>
            <a class="btn btn-secondary" href="/admin/users"><i class="fas fa-times"></i> Cancel</a>
        </div>
    </div>

    </form>
    <!--- end form --->

    </div>
    </section>
</cfoutput>

<!--- scripts --->
<script type="text/javascript">
//load on document ready//////////////////////////
$(function(){

    //general variables---------------------------
    var user = <cfset writeOutput(serializeJSON(oUser)) />;
    var arrCountries = <cfset writeOutput(serializeJSON(stData.countries)) />;
    var arrStates = <cfset writeOutput(serializeJSON(stData.states)) />;
    var USCountryID, CACountryID;

    //get US and CA country IDs
    $.each(arrCountries, function(i,v) {
        if (v.iso === "US"){
            USCountryID = v.id;
        } else if (v.iso === "CA"){
            CACountryID = v.id;
        } else{
            return true;
        }
    });

    //form control selectors----------------------
    var userForm = $("#form_user");
    var userFormUsernameField = $("#form_user-input_username");
    var userFormEmailField = $("#form_user-input_email");
    var userFormStateField = $("#form_user-input_state");
    var userFormAddressLine1Field = $("#form_user-input_shippingAddress_line1");
    var userFormAddressLine2Field = $("#form_user-input_shippingAddress_line2");
    var userFormOtherStateField = $("#form_user-input_otherState");
    var userFormCountryField = $("#form_user-input_country");

    //pre-populate form fields
    userFormUsernameField.val(user.username);
    userFormEmailField.val(user.email);
    if(user.id){userFormCountryField.val(user.countryID)};
    userFormAddressLine1Field.val(user.shippingAddressLine1);
    userFormAddressLine2Field.val(user.shippingAddressLine2);

    //switch state field depending on country-----
    $.when(
        userFormCountryField.on("change", function() {
            var countryID = parseInt($(this).val());

            //hide state fields
            userFormStateField.hide();
            userFormOtherStateField.hide();

            //first determine whether to show a dropdown or text
            // if not US/CS, show text
            if ([USCountryID,CACountryID].indexOf(countryID) < 0){
                userFormOtherStateField.show();

                //else, if it is US/CA
            } else {

                //empty the dropdown before populating with corresponding states
                userFormStateField.empty();

                //loop through the object containing US/CA states
                $.each(arrStates, function(i,v){

                    //filter only states belonging to the currently selected country
                    if (v.countryID === countryID){

                        //add each of the selected country's state to the dropdown
                        userFormStateField.append('<option value="' + v.id + '">' + v.name + '</option>');

                    }

                });

                userFormStateField.show();

            }

        }) //trigger change on document load as well.
    ).then(function(){
        userFormCountryField.change();
        if ([USCountryID,CACountryID].indexOf(user.countryID) < 0) {
            userFormOtherStateField.val(user.state);
        } else {
            $.each(arrStates,function(i,v){
                if (v.name === user.state) {
                    userFormStateField.val(v.id);
                    return false;
                } else {
                    return true;
                }
            });
            userFormStateField.val();
        }
    });

    //user form submit----------------------------
    userForm.on("submit", function(e){

        e.preventDefault();

        $.post(
            "/account/edit/ajax/index.cfm",
            userForm.serialize()
        )
        .done(function(data){
            document.location = "/account";
        })
        .fail(function(response){
            alert(response.getResponseHeader("x-error-message"));
        });

    });

});
</script>