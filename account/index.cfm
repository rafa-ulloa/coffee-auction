<!--- get data --->
<cfset stData = application.userController.viewUser(request.user.getID()) />
<cfset oUser = stData.user />

<!--- header --->
<header class="row">
    <div class="col-sm">
        <h1>Account</h1>
    </div>
</header>

<!--- start columns --->
<cfoutput>
<section class="row" id="home">

    <div class="col-sm-12 col-md-6">
        <dl class="row">

            <!--- username --->
            <dt class="col-sm-3">Username</dt>
            <dd class="col-sm-9">#oUser.getUsername()#</dd>

            <!--- email --->
            <dt class="col-sm-3">E-Mail</dt>
            <dd class="col-sm-9">#oUser.getEmail()#</dd>

            <!--- country --->
            <dt class="col-sm-3">Country</dt>
            <dd class="col-sm-9">
                <cfloop array="#stData.countries#" index="i" item="it">
                    <cfif it.getID() eq oUser.getCountryID()>
                        #it.getName()#
                        <cfbreak />
                    <cfelse>
                        <cfcontinue />
                    </cfif>
                </cfloop>
            </dd>

            <!--- state --->
            <dt class="col-sm-3">State</dt>
            <dd class="col-sm-9">#oUser.getState()#</dd>

            <!--- shipping address --->
            <dt class="col-sm-3">Shipping Address</dt>
            <dd class="col-sm-9">#oUser.getShippingAddressLine1()#</dd>

            <!--- shipping address --->
            <dt class="col-sm-3">Shipping Address (line 2)</dt>
            <dd class="col-sm-9">#oUser.getShippingAddressLine2()#</dd>

        </dl>
        <div class="row">
            <div class="col-sm-12 text-center">
                <a class="btn btn-primary" href="/account/edit"><i class="fas fa-edit"></i> Edit Account</a>
                <a class="btn btn-secondary" href="/account/password"><i class="fas fa-key"></i> Change Password</a>
            </div>
        </div>
    </div>

    <!--- right column --->
    <div class="col-sm-12 col-md-6">
    	<div class="row justify-content-center">
		<a href="auctions" class="btn btn-primary"><i class="fas fa-gavel"></i> View My Auctions</a>
	</div>
    </div>

</section>
</cfoutput>
