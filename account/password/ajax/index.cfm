<cfparam type="numeric" name="form.userID" default="0" />
<cfparam type="string" name="form.password" default="" />
<cfparam type="string" name="form.confirmPassword" default="" />

<!--- start try --->
<cftry>

  <cfset application.userController.changePassword(argumentCollection = form) />

  <!--- start error handling --->
  <cfcatch type="any">
    <cfheader statuscode="501" />
    <cfheader name="x-error-message" value="#cfcatch.message#" />
    <cfheader name="x-error-detail" value="#cfcatch.detail#" />
  </cfcatch>

</cftry>
<!--- end try --->