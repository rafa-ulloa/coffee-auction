<!--- header --->
<header class="row">
    <div class="col-sm">
        <h1>Change Password</h1>
    </div>
</header>

<!--- start login content --->
<section class="row justify-content-center">
<div class="col-sm-12 col-md-6">

    <!--- start form --->
    <form id="form_password">

        <!--- userID --->
        <input type="hidden" id="form_password-input_userID" name="userID" />

        <!--- username --->
        <div class="form-group row">
            <label for="form_password-password" class="col-sm-3 col-form-label">New Password</label>
            <div class="col-sm-9">
                <input type="password" class="form-control" id="form_password-input_password" name="password" />
            </div>
        </div>

        <!--- password --->
        <div class="form-group row">
            <label for="form_password-input_confirmPassword" class="col-sm-3 col-form-label">Confirm New Password</label>
            <div class="col-sm-9">
                <input type="password" class="form-control" id="form_password-input_confirmPassword" name="confirmPassword" />
            </div>
        </div>

        <!--- submit --->
        <div class="form-group row">
            <div class="col-sm-12 text-center">
                <button type="submit" class="btn btn-primary" name="submit" value="submit"><i class="fas fa-key"></i> Change Password</button>
            </div>
        </div>

    </form>
    <!--- end form --->

</div>
</section>

<!--- scripts --->
<script type="text/javascript">
//load on document ready//////////////////////////
$(function(){

    //general variables
    var userID = <cfset writeOutput(request.user.getID()) />;

    //form fields
    var passwordForm = $("#form_password");
    var passwordFormUserIDField = $("#form_password-input_userID");

    //populate userID field
    passwordFormUserIDField.val(userID);

    //form submit
    passwordForm.on("submit", function(e){

        //stop form from natural submit
        e.preventDefault();

        //make ajax request
        $
            .post(
                "/account/password/ajax/index.cfm",
                passwordForm.serialize()
            )
            .done(function(){
                window.location = "/account"
            })
            .fail(function(response){
                alert(response.getResponseHeader("x-error-message"));
            });

    });

});
</script>