<cfcomponent>
	
  <!--- settings///////////////////////////////////////--->
  <cfset this.name = "coffeeAuctionAdmin">
  <cfset this.clientManagement = 1>

  <!--- public methods/////////////////////////////////--->

  <!--- function: onApplicationStart---------------------->
  <cffunction name="onApplicationStart">
    <cfset application.datasource = "coffee_auction" />
    <cfset application.restartKey = "abracadabra" />
    <cfset application.mailAddress = "rafael.ulloa@mac.com" />
    <cfset application.bidRaiseMin = 100 />

    <!--- cache controllers --->
    <cfset application.userController = new controllers.userController(
      new com.dao.UserDAO(application.datasource),
      new com.dao.CountryDAO(application.datasource),
      new com.dao.StatesDAO(application.datasource)
    ) />
    <cfset application.adminController = new controllers.adminController(
	new com.dao.adminDAO(application.datasource)
    ) />
    <cfset application.auctionController = new controllers.auctionController(
      new com.dao.AuctionDAO(application.datasource),
      new com.dao.MerchantDAO(application.datasource),
      new com.dao.UserDAO(application.datasource),
      new com.dao.BidDAO(application.datasource),
      application.bidRaiseMin
    ) />

  </cffunction>
  
  <!--- function: onRequestStart-------------------------->
  <cffunction name="onRequestStart">
	<cfparam type="string" name="url.restartKey" default="" />
    <cfparam type="string" name="request.message" default="" />
    <cfparam type="string" name="client.requestMessage" default="" />
    <cfparam type="string" name="client.admin" default="#serializeJSON(new com.users.Admin(0,"","",now()))#" />

	<!--- check for restart key --->
    <cfif url.restartKey eq application.restartKey>
      <cfset onApplicationStart()>
    </cfif>
        
	<!--- set request variables --->
    <cfset request.section = listLast(getDirectoryFromPath(getBaseTemplatePath()), "/\")>
    <cfset request.isLandingPage = listContainsNoCase('wwwroot', request.section)>
  	<cfset request.isAjaxRequest = request.section eq "ajax">
    <cfset request.admin= new com.users.Admin(argumentCollection = deserializeJSON(client.admin)) />

    <!--- if user is not logged in.. --->
    <cfif !request.admin.getID() && !listContains("login,ajax", request.section)>
      <cflocation url="/admin/login" addtoken="0" />
    </cfif>

    <!--- do not include header if it is an ajax request --->
    <cfif !request.isAjaxRequest>
  		<cfinclude template="/admin/includes/header.cfm">
    </cfif>
    
    <!--- relay any application messages --->
    <cfif len(client.requestMessage)>
      <cfset request.message = client.requestMessage>
      <cfset client.requestMessage = "">
    </cfif>
    
  </cffunction>

  <!--- function: onRequestEnd---------------------------->
  <cffunction name="onRequestEnd">
  	<cfif !request.isAjaxRequest>
  		<cfinclude template="/admin/includes/footer.cfm">
    </cfif>
  </cffunction>
  
</cfcomponent>
