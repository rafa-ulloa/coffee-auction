<cfparam type="numeric" name="blockFactor" default="50" />
<cfparam type="numeric" name="blockOffset" default="0" />
<cfset stData = application.auctionController.searchAuctions(argumentCollection = variables) />

<!--- header --->
<header class="row">
    <div class="col-sm clearfix">
        <h1 class="float-left">Auctions</h1>
        <a class="btn btn-primary float-right" href="update" role="button"><i class="fas fa-gavel"></i> Create Auction</a>
    </div>
</header>

<!--- list users --->
<div class="row">
<div class="col-sm">
<table class="table">
    <thead>
    <tr>
        <th scope="col">id</th>
        <th scope="col">Start Date</th>
        <th scope="col">End Date</th>
        <th scope="col">Merchant</th>
        <th scope="col">Date Created</th>
        <th scope="col">Actions</th>
    </tr>
    </thead>
<tbody>
<cfoutput>
    <cfloop array="#stData.results#" index="i" item="it">
    <tr>
        <td>#it.getID()#</td>
        <td>#dateFormat(it.getStartDate(), "short")#</td>
        <td>#dateFormat(it.getEndDate(), "short")#</td>
        <td>
            <cfloop array="#stData.merchants#" index="j" item="jt">
                <cfif it.getMerchantID() eq jt.getID()>
                    <cfset writeOutput(jt.getFullName()) />
                    <cfbreak />
                <cfelse>
                    <cfcontinue />
                </cfif>
            </cfloop>
        </td>
      <td>#dateFormat(it.getDateCreated(), "short")#</td>
      <td><a class="btn btn-dark" href="update?id=#it.getID()#" role="button"><i class="fas fa-pencil-alt"></i> Edit</a></td>
    </tr>
    </cfloop>
</cfoutput>
</tbody>
</table>
</div>
</div>
