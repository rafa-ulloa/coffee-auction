<!--- get data --->
<cfparam type="numeric" name="form.id" default="0" />
<cfparam type="numeric" name="form.adminID" default="0" />
<cfparam type="date" name="form.startDate" default="#now()#" />
<cfparam type="date" name="form.endDate" default="#now()#" />
<cfparam type="numeric" name="form.merchantID" default="0" />
<cfparam type="string" name="form.description" default="" />
<cfparam type="numeric" name="form.startingPrice" default="0" />

<!--- start try --->
<cftry>

  <cfset application.auctionController.updateAuction(argumentCollection = form) />

<!--- start error handling --->
  <cfcatch type="any">
    <cfheader statuscode="501" />
    <cfheader name="x-error-message" value="#cfcatch.message#" />
    <cfheader name="x-error-detail" value="#cfcatch.detail#" />
  </cfcatch>

</cftry>
<!--- end try --->
