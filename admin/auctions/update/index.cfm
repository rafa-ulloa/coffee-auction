<!--- get data --->
<cfparam type="numeric" name="url.id" default="0" />
<cfset stData = application.auctionController.viewAuction(url.id) />
<cfset oAuction = stData.auction />
<cfset arrMerchants = stData.merchants />

<!--- header --->
<header class="row">
<div class="col-sm clearfix">
    <h1 class="float-left"><cfset writeOutput(oAuction.getID() ? "Edit" : "Create") /> Auction</h1>
    <a class="btn btn-primary float-right" href="/admin/users" role="button"><i class="fas fa-arrow-left"></i> Back</a>
</div>
</header>

<!--- start form --->
<cfoutput>
    <section class="row justify-content-center">
    <div class="col-sm-12 col-md-6">

        <!--- start form --->
        <form id="form_auction">

        <!--- hidden fields --->
        <input type="hidden" name="id" id="form_auction-input_id">
        <input type="hidden" name="adminID" id="form_auction-input_adminID">

        <!--- start date --->
        <div class="form-group row">
            <label for="form_auction-input_startDate" class="col-sm-3 col-form-label">Start date</label>
             <div class="col-sm-9">
                <input type="date" class="form-control" id="form_auction-input_startDate" name="startDate">
            </div>
        </div>

        <!--- end date --->
        <div class="form-group row">
            <label for="form_auction-input_endDate" class="col-sm-3 col-form-label">End date</label>
            <div class="col-sm-9">
                <input type="date" class="form-control" id="form_auction-input_endDate" name="endDate">
            </div>
        </div>

        <!--- merchant --->
        <div class="form-group row">
            <label for="form_auction-input_merchant" class="col-sm-3 col-form-label">Merchant</label>
            <div class="col-sm-9">
                <select class="form-control" id="form_auction-input_merchantID" name="merchantID">
                    <option value="0">select one...</option>
                </select>
            </div>
        </div>

        <!--- description --->
        <div class="form-group row">
            <label for="form_auction-input_description" class="col-sm-3 col-form-label">Description</label>
            <div class="col-sm-9">
                <textarea class="form-control" id="form_auction-input_description" name="description"></textarea>
            </div>
        </div>

        <!--- starting price --->
        <div class="form-group row">
            <label for="form_auction-input_startingPrice" class="col-sm-3 col-form-label">Starting Price (USD)</label>
            <div class="col-sm-9">
                <input type="number" class="form-control" id="form_auction-input_startingPrice" name="startingPrice" min="0" />
            </div>
        </div>

        <!--- submit --->
        <div class="form-group row">
            <div class="col-sm-12 text-center">
                <button type="submit" class="btn btn-primary" name="submit" value="submit"><i class="fas fa-check"></i> Submit</button>
                <a class="btn btn-secondary" href="/admin/auctions"><i class="fas fa-times"></i> Cancel</a>
            </div>
        </div>

    </form>
    <!--- end form --->

    </div>
    </section>
</cfoutput>

<!--- scripts --->
<script type="text/javascript">
//load on document ready//////////////////////////
$(function(){

    //general variables---------------------------
    var auction = <cfset writeOutput(serializeJSON(oAuction)) />;
    var auctionStartDate = <cfset writeOutput(DE(dateFormat(oAuction.getStartDate(), "yyyy-MM-dd"))) />;
    var auctionEndDate = <cfset writeOutput(DE(dateFormat(oAuction.getEndDate(), "yyyy-MM-dd"))) />;
    var arrMerchants = <cfset writeOutput(serializeJSON(arrMerchants)) />;

    //form control selectors----------------------
    var auctionForm = $("#form_auction");
    var auctionFormStartDateField = $("#form_auction-input_startDate");
    var auctionFormEndDateField = $("#form_auction-input_endDate");
    var auctionFormMerchantsField = $("#form_auction-input_merchantID");
    var auctionFormIDField = $("#form_auction-input_id");
    var auctionFormAdminIDField = $("#form_auction-input_adminID");
    var auctionFormDescriptionField = $("#form_auction-input_description");
    var auctionFormStartingPriceField = $("#form_auction-input_startingPrice");

    //setup options for merchants
    $.when(
        $.each(arrMerchants, function(i,v){
            var option = $("<option></option>");
            option
                .attr("value", v.id)
                .text(v.firstName + " " + v.lastName);
            auctionFormMerchantsField.append(option);
        })
    )
    .then(function(){  //pre-populate merchant once done
        auctionFormMerchantsField.val(auction.merchantID);
    });


    //pre-populate form fields
    auctionFormStartDateField.val(auctionStartDate);
    auctionFormEndDateField.val(auctionEndDate);
    auctionFormIDField.val(auction.id);
    auctionFormAdminIDField.val(auction.adminID);
    auctionFormDescriptionField.val(auction.description);
    auctionFormStartingPriceField.val(auction.startingPrice);


    //user form submit----------------------------
    auctionForm.on("submit", function(e){

        e.preventDefault();

        $.post(
            "/admin/auctions/update/ajax/index.cfm",
            auctionForm.serialize()
        )
        .done(function(data){
            document.location = "/admin/auctions";
        })
        .fail(function(response){
            alert(response.getResponseHeader("x-error-message"));
        });

    });

});
</script>
