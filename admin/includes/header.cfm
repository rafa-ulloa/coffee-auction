<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
    <title>Coffee Auction</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/app.css">
    <script src="/js/jquery-3.3.1.min.js"></script>
    <script src="/js/popper.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/admin.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
</head>
<body>

<!--- start navbar --->
<nav class="navbar navbar-expand-lg navbar-light bg-light">

    <!--- logo/company name --->
    <a class="navbar-brand" href="/admin">Coffee Auction</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <!--- nav links --->
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
            <cfif request.admin.getID()>
                <li class="nav-item">
                    <a class="nav-link" href="/admin/users">Users</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/admin/auctions">Auctions</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/admin/merchants">Merchants</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/admin/update?id=0">rulloa</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-secondary" href="?restartKey=abracadabra" role="button"><i class="fas fa-sync"></i> Reload Admin</a>
                    <a class="btn btn-primary" href="/admin/logout" role="button"><i class="fas fa-sign-out-alt"></i> Log Out</a>
                </li>
            <cfelse>
                <li class="nav-item">
                    <a class="nav-link" href="/admin/login">Log In</a>
                </li>
            </cfif>
        </ul>
    </div>
</nav>
<!--- end navbar --->

<!--- start content --->
<div class="container-fluid">
