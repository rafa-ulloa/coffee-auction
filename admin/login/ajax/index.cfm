<cfparam type="string" name="username" default="" />
<cfparam type="string" name="password" default="" />

<!--- start try --->
<cftry>

  <cfset application.adminController.loginAdmin(argumentCollection = form) />

  <!--- start error handling --->
  <cfcatch type="any">
    <cfheader statuscode="501" />
    <cfheader name="x-error-message" value="#cfcatch.message#" />
    <cfheader name="x-error-detail" value="#cfcatch.detail#" />
  </cfcatch>

</cftry>
<!--- end try --->
