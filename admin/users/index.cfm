<cfparam type="numeric" name="blockFactor" default="50" />
<cfparam type="numeric" name="blockOffset" default="0" />
<cfset stData = application.userController.searchUsers(argumentCollection = variables) />

<!--- header --->
<header class="row">
    <div class="col-sm clearfix">
        <h1 class="float-left">Users</h1>
        <a class="btn btn-primary float-right" href="update" role="button"><i class="fas fa-user-plus"></i> Create User</a>
    </div>
</header>

<!--- list users --->
<div class="row">
    <div class="col-sm">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Username</th>
                <th scope="col">Email</th>
                <th scope="col">Country</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
            <cfoutput>
            <cfloop array="#stData.results#" index="i" item="it">
                <tr>
                    <td>#it.getUsername()#</td>
                    <td>#it.getEmail()#</td>
                    <td>
                        <cfloop array="#stData.countries#" index="j" item="jt">
                            <cfif jt.getID() eq it.getCountryID()>
                                #jt.getName()#
                                <cfbreak/>
                            </cfif>
                        </cfloop>
                    </td>
                    <td><a class="btn btn-dark" href="update?id=#it.getID()#" role="button"><i class="fas fa-pencil-alt"></i> Edit</a></td>
                </tr>
            </cfloop>
            </cfoutput>
            </tbody>
        </table>
    </div>
</div>