<!--- get data --->
<cfparam type="numeric" name="form.id" default="0" />
<cfparam type="numeric" name="form.auctionID" default="0" />
<cfparam type="numeric" name="form.userID" default="0" />
<cfparam type="date" name="form.amount" default="0" />

<!--- start try --->
<cftry>

  <cfset application.auctionController.placeBidOnAuction(argumentCollection = form) />

<!--- start error handling --->
  <cfcatch type="any">
    <cfheader statuscode="501" />
    <cfheader name="x-error-message" value="#cfcatch.message#" />
    <cfheader name="x-error-detail" value="#cfcatch.detail#" />
  </cfcatch>

</cftry>
<!--- end try --->
