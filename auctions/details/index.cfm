<!--- get data --->
<cfparam type="numeric" name="url.id" default="0" />
<cfset stData = application.auctionController.viewAuction(url.id) />
<cfset oAuction = stData.auction />
<cfset arrMerchants = stData.merchants />
<cfset currentBiddingPrice = stData.highestBid.getAmount() ? stData.highestBid.getAmount() : oAuction.getStartingPrice() />
<cfset raiseBiddingPrice = currentBiddingPrice + stData.bidRaiseMin />

<!--- header --->
<header class="row">
<div class="col-sm clearfix">
    <h1 class="float-left">Auction <cfset writeOutput(chr(35) & chr(32) & url.id) /></h1>
    <a class="btn btn-primary float-right" href="/auctions" role="button"><i class="fas fa-arrow-left"></i> Back</a>
</div>
</header>

<!--- start form --->
<cfoutput>
    <section class="row justify-content-center">

    <!--- merchant info --->
    <div class="col-sm-12 col-md-6">
        <cfloop array="#arrMerchants#" index="i" item="it">
            <cfif oAuction.getMerchantID() eq it.getID()>
                <dl>
                    <dt>Merchant</dt>
                    <dd>#it.getFirstName() & chr(32) & it.getLastName()#</dd>
                </dl>
            </cfif>
        </cfloop>
    </div>

    <!--- auction info --->
    <div class="col-sm-12 col-md-6">

        <dl>
            <dt>Start Date</dt>
            <dd>#datetimeFormat(oAuction.getStartDate(), "long")#</dd>
            <dt>End Date</dt>
            <dd>#datetimeFormat(oAuction.getEndDate(), "long")#</dd>
            <dt>Current Bidding Price</dt>
            <dd>$#currentBiddingPrice#</dd>
            <dt>Auction Ends in</dt>
            <dd id="countdown"></dd>
        </dl>

        <p>#oAuction.getDescription()#</p>

        <div class="text-center">
		<cfif dateCompare(oAuction.getStartDate(), now(), "d") gt 0>
			<div class="alert alert-primary" role="alert">This auction begins on #dateFormat(oAuction.getStartDate(), "short")#</div>
		<cfelseif dateCompare(oAuction.getEndDate(), now(), "d") lt 0>
			<div class="alert alert-primary" role="alert">This auction has ended.</div>
		<cfelse>
			<cfif request.user.getID()>
				<cfif request.user.getID() neq stData.highestBidder.getID()>
					<a class="btn btn-primary" href="##" data-toggle="modal" data-target="##modalConfirmBid" role="button"><i class="fas fa-gavel"></i> Bid $<span class="bidAmount"></span></a>
				<cfelse>
					<div class="alert alert-primary" role="alert">You are currently the highest bidder.</div>
				</cfif>
			<cfelse>
				<a class="btn btn-primary" href="/login" role="button"><i class="fas fa-sign-in-alt"></i> Sign in to bid</a>
			</cfif>
		</cfif>
        </div>

    <!--- end form --->

    </div>
</section>

<!---modal--->
<div class="modal fade" id="modalConfirmBid" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
	            		<h5 class="modal-title">Confirm bid</h5>
		            	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			        	<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body text-center">
				<p>Place your bid for $<span class="bidAmount"></span>?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="btnConfirmBid">Place bid</button>
			</div>
		</div>
	</div>
</div>
</cfoutput>

<!--- scripts --->
<script type="text/javascript">
//load on document ready//////////////////////////
$(function(){

	var stData = <cfset writeOutput(serializeJSON(stData)) />;
	var currentBiddingPrice = stData.HIGHESTBID ? stData.HIGHESTBID.amount : stData.AUCTION.startingPrice;
	var nextBiddingPrice = currentBiddingPrice + stData.BIDRAISEMIN;
	var endDateStr = <cfset writeOutput(DE(datetimeFormat(stData.auction.getEndDate(), "full"))) />
	let user = <cfset writeOutput(serializeJSON(request.user)) />;

	//init countdown
	$("#countdown").countdown(new Date(endDateStr), function(event){
		$(this).html(event.strftime("%D days %H:%M:%S"));
	});
	
	//populate next bidding amount
	$(".bidAmount").text(nextBiddingPrice);

	//place bid
	$("#btnConfirmBid").on("click", function(){
		
		//do ajax post
		$.post(
			'/auctions/details/ajax/index.cfm',
			{id : 0, auctionID : stData.AUCTION.id, userID : user.id, amount : nextBiddingPrice}
		)
		.done(function(response){
			window.location.reload(1);
		})
		.fail(function(response){
			alert(response.getResponseHeader("x-error-message"));
		});

	});

});
</script>
