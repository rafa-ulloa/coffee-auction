<!--- get data --->
<cfparam type="numeric" name="blockFactor" default="50" />
<cfparam type="numeric" name="blockOffset" default="0" />
<cfset stData = application.auctionController.searchAuctions(argumentCollection = variables) />

<!--- header --->
<header class="row">
    <div class="col-sm">
        <h1>Auctions</h1>
    </div>
</header>

<!--- start auctions --->
<cfoutput>
<section class="row" id="about">

    <!--- loop through auctions --->
    <cfloop array="#stData.results#" index="i" item="it">
        <div class="col-sm-12 col-md-4 col-lg-3">
            <div class="card">
                <img class="card-img-top" src="http://via.placeholder.com/350x150" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Auction #chr(35) & it.getID()#</h5>
                    <p class="card-text">#it.getDescription()#</p>
                    <a href="/auctions/details?id=#it.getID()#" class="btn btn-primary"><i class="fas fa-eye"></i> Auction details</a>
                </div>
            </div>
        </div>
    </cfloop>

</section>
</cfoutput>
<!--- end auctions --->
