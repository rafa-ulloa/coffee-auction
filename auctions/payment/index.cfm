<!--- get data --->
<cfparam name="url.id" type="numeric" default="0" />
<cfset stData = application.auctionController.viewAuction(url.id) />

<!--- header --->
<div class="row">
	<div class="col-sm">
		<h1>Payment</h1>
	</div>
</div>

<!--- start content --->
<section class="row">

	<!--- auction info --->
	<div class="col-sm-12 col-md-6" id="column-auctionInfo">
		<dl class="row">

			<!--- auction id --->
			<dt class="col-sm-4">Auction No.</dt>
			<dd class="col-sm-8" id="auctionInfo-id"></dd>

			<!--- auction started --->
			<dt class="col-sm-4">Auction Started</dt>
			<dd class="col-sm-8" id="auctionInfo-startDate"></dd>

			<!--- auction ended --->
			<dt class="col-sm-4">Auction Ended</dt>
			<dd class="col-sm-8" id="auctionInfo-endDate"></dd>

			<!--- auction merchant --->
			<dt class="col-sm-4">Merchant</dt>
			<dd class="col-sm-8" id="auctionInfo-merchant"></dd>
			
			<!--- auction description --->
			<dt class="col-sm-4">Description</dt>
			<dd class="col-sm-8" id="auctionInfo-description"></dd>

		</dl>
	</div>

	<!--- payment info --->
	<div class="col-sm-12 col-md-6" id="column-invoice">

		<dl class="row">

			<!--- amount --->
			<dt class="col-sm-4">Bidding Amount</dt>
			<dd class="col-sm-8" id="auctionPayment-amount"></dd>

			<!--- taxes --->
			<dt class="col-sm-4">Taxes</dt>
			<dd class="col-sm-8" id="auctionPayment-taxes"></dd>

			<!--- shipping --->
			<dt class="col-sm-4">Shipping &amp; handling</dt>
			<dd class="col-sm-8" id="auctionPayment-shipping"></dd>

			<!--- total --->
			<dt class="col-sm-4 border-top">Total</dt>
			<dd class="col-sm-8 border-top font-weight-bold" id="auctionPayment-total"></dd>

		</dl>

		<div class="alert alert-danger text-center d-none" role="alert"></div>
		<form id="paymentForm" action="" method="post">
			<input type="hidden" name="cmd" value="_xclick">
			<input type="hidden" name="business" value="HMYXBLTVYBSKS" />
			<input type="hidden" name="bn" value="greenbox_BuyNow_WPS_HN" />
			<input type="hidden" name="amount" value="" />
			<input type="hidden" name="item_name" value="" />
			<input type="hidden" name="item_number" value="" />
			<input type="hidden" name="quantity" value="" />
			<input type="hidden" name="shipping" value="" />
			<input type="hidden" name="tax" value="" />
			<input type="hidden" name="no_shipping" value="1" />
			<input type="hidden" name="return" value="" />
			<input type="hidden" name="rm" value="2" />
			<input type="hidden" name="cancel_return" value="" />
			<button class="btn btn-primary d-none btn-block" id="btn-paynow"><i class="fab fa-paypal"></i> Pay Now</button>
		</form>

	</div>

</section>

<script type="text/javascript">
//on document load/////////////////////////////////////////
$(function(){

	//get data
	<cfoutput>
	var stData = #serializeJSON(stData)#;
	var auctionStartDateString = #DE(dateFormat(stData.auction.getStartDate(), "yyyy-mm-dd"))#;
	var auctionStartDate = new Date(auctionStartDateString);
	var auctionEndDateString = #DE(dateFormat(stData.auction.getEndDate(), "yyyy-mm-dd"))#;
	var auctionEndDate = new Date(auctionEndDateString);
	var today = new Date();
	var user = #serializeJSON(request.user)#;
	var merchant;
	var taxes = 10.00;
	var shippingCost = 20.23;
	var total = stData.HIGHESTBID.amount + taxes + shippingCost;
	</cfoutput>

	//get merchant for auction
	$.each(stData.MERCHANTS, function(i,v){
		if (v.id === stData.AUCTION.merchantID){
			merchant = v;
			return false;
		}
		else {
			return true;	
		}
	});

	//set vars
	var errorMessage = "";
	var errorMessageContainer = $(".alert");
	var auctionInfoColumn = $("#column-auctionInfo");
	var invoiceColumn = $("#column-invoice");
	var auctionInfoID = $("#auctionInfo-id");
	var auctionInfoStartDate = $("#auctionInfo-startDate");
	var auctionInfoEndDate = $("#auctionInfo-endDate");
	var auctionInfoDescription = $("#auctionInfo-description");
	var auctionInfoMerchant = $("#auctionInfo-merchant");
	var auctionPaymentAmount = $("#auctionPayment-amount");
	var auctionPaymentTaxes = $("#auctionPayment-taxes");
	var auctionPaymentShipping = $("#auctionPayment-shipping");
	var auctionPaymentTotal = $("#auctionPayment-total");
	var buttonPayNow = $("#btn-paynow");
	var paymentForm = $("#paymentForm");
	var paymentForm_amountField = $("input[name=amount]");
	var paymentForm_itemNameField = $("input[name=item_name]");
	var paymentForm_itemNumberField = $("input[name=item_number]");
	var paymentForm_quantityField = $("input[name=quantity]");
	var paymentForm_shippingField = $("input[name=shipping]");
	var paymentForm_taxField = $("input[name=tax]");
	var paymentForm_returnField = $("input[name=return]");
	var paymentForm_cancelReturnField = $("input[name=cancel_return]");
	var paymentForm_actionURL = "https://www" + (location.hostname.includes("virtualcoffeeauctions.com") ? "" :".sandbox") + ".paypal.com/cgi-bin/webscr";

	//validations
	if (!stData.AUCTION.id){
		errorMessage = "No auction was specified for payment. Make sure the auction ID parameter is passed.";
	}
	else if (auctionEndDate > today) {
		errorMessage = "The auction has not ended.";
	}
	else if (!user.id){
		errorMessage  ="You must be logged in to pay for this auction.";
	}
	else if (stData.AUCTION.isPaid){
		errorMessage = "This auction has already been paid for.";
	}
	else if (stData.HIGHESTBIDDER.id !== user.id){
		errorMessage = "You are not the winning bidder. You cannot pay for this auction.";
	}

	//show message if an error was found, otherwise show pay now button
	if (errorMessage.length){
		errorMessageContainer.text(errorMessage).removeClass("d-none");
	}
	else {
		buttonPayNow.removeClass("d-none");
	}

	//populate auction info
	auctionInfoID.text(stData.AUCTION.id);
	auctionInfoStartDate.text(auctionStartDateString);
	auctionInfoEndDate.text(auctionEndDateString);
	auctionInfoDescription.text(stData.AUCTION.description);
	auctionInfoMerchant.text(merchant.firstName + " " + merchant.lastName);
	
	//populate payment info
	auctionPaymentAmount.text(stData.HIGHESTBID.amount);
	auctionPaymentTaxes.text(taxes);
	auctionPaymentShipping.text(shippingCost);
	auctionPaymentTotal.text(total + " USD");

	//populate payment form fields
	paymentForm.attr("action", paymentForm_actionURL);
	paymentForm_amountField.val(stData.HIGHESTBID.amount);
	paymentForm_itemNameField.val("Coffee Auction No. " + stData.AUCTION.id);
	paymentForm_itemNumberField.val(stData.AUCTION.id);
	paymentForm_quantityField.val(1);
	paymentForm_shippingField.val(shippingCost);
	paymentForm_taxField.val(taxes);
	paymentForm_returnField.val(window.location.origin + "/auctions/thank-you/?paymentMethod=paypal&id=" + stData.AUCTION.id);
	paymentForm_cancelReturnField.val(window.location.origin + "/auctions/payment/?id=" + stData.AUCTION.id);

});
</script>
