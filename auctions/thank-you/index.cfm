<!--- get values --->
<cfparam name="url.id" type="numeric" default="0" />
<cfparam name="url.paymentMethod" type="string" default="" />
<cfparam name="form.item_name" type="string" default="the coffee auction" />
<cfparam name="form.txn_id" type="string" defaul="" />

<!--- add payment record --->
<cfset application.auctionController.setAuctionAsPaid(url.id, url.paymentMethod, form.txn_id) />

<!--- header --->
<div class="row">
	<div class="col-sm text-center">
		<h1>Thank you!</h1>
	</div>
</div>

<!--- start content --->
<section class="row">

	<div class="col-sm text-center">
		<h2>Your Payment for <cfset writeOutput(form.item_name) /> has been completed. <br />
		We will begin shipment for your coffee lot.</h2>
		<a href="/auctions" class="btn btn-primary"><i class="fas fa-gavel"></i> Go to Auctions</a>
	</div>

</section>
