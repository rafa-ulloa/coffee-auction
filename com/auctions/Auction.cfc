<cfcomponent accessors="1">

<!--- properties //////////////////////////////////--->
    <cfproperty name="id" type="numeric" />
    <cfproperty name="startDate" type="date" />
    <cfproperty name="endDate" type="date" />
    <cfproperty name="merchantID" type="numeric" />
    <cfproperty name="dateCreated" type="date" />
    <cfproperty name="adminID" type="numeric" />
    <cfproperty name="description" type="string" />
    <cfproperty name="startingPrice" type="numeric" />
    <cfproperty name="isPaid" type="boolean" />
    <cfproperty name="paymentMethodID" type="numeric" />
    <cfproperty name="paymentConfirmationNumber" type="string" />

<!--- public methods //////////////////////////////--->

<!--- function: init --------------------------------->
    <cffunction name="init" returntype="Auction">
        <cfargument name="id" type="numeric" required="0" default="0" />
        <cfargument name="startDate" type="date" required="0" default="#now()#" />
        <cfargument name="endDate" type="date" required="0" default="#now()#" />
        <cfargument name="merchantID" type="numeric" required="0" default="0" />
        <cfargument name="dateCreated" type="date" required="0" default="#now()#" />
        <cfargument name="adminID" type="numeric" required="0" default="1" />
        <cfargument name="description" type="string" required="0" default="" />
        <cfargument name="startingPrice" type="numeric" required="0" default="0" />
        <cfargument name="isPaid" type="boolean" required="0" default="0" />
        <cfargument name="paymentMethodID" type="string" required="0" default="" />
        <cfargument name="paymentConfirmationNumber" type="string" required="0" default="" />
        <cfset setID(arguments.id) />
        <cfset setStartDate(arguments.startDate) />
        <cfset setEndDate(arguments.endDate) />
        <cfset setMerchantID(arguments.merchantID) />
        <cfset setDateCreated(arguments.dateCreated) />
        <cfset setAdminID(arguments.adminID) />
        <cfset setDescription(arguments.description) />
        <cfset setStartingPrice(arguments.startingPrice) />
        <cfset setIsPaid(arguments.isPaid) />
        <cfset setPaymentMethodID(len(arguments.paymentMethodID) ? arguments.paymentMethodID : 0) />
        <cfset setPaymentConfirmationNumber(arguments.paymentConfirmationNumber) />
        <cfreturn this />
    </cffunction>

</cfcomponent>
