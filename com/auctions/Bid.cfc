<cfcomponent accessors="1">

    <!--- properties //////////////////////////////////--->
    <cfproperty name="id" type="numeric" />
    <cfproperty name="auctionID" type="numeric" />
    <cfproperty name="userID" type="numeric" />
    <cfproperty name="amount" type="numeric" />
    <cfproperty name="dateCreated" type="date" />

    <!--- public methods //////////////////////////////--->

    <!--- function: init --------------------------------->
    <cffunction name="init" returntype="Bid">
        <cfargument name="id" type="numeric" required="0" default="0" />
        <cfargument name="auctionID" type="numeric" required="0" default="0" />
        <cfargument name="userID" type="numeric" required="0" default="0" />
        <cfargument name="amount" type="numeric" required="0" default="0" />
        <cfargument name="dateCreated" type="date" required="0" default="#now()#" />
        <cfset setID(arguments.id) />
        <cfset setAuctionID(arguments.auctionID) />
        <cfset setUserID(arguments.userID) />
        <cfset setAmount(arguments.amount) />
        <cfset setDateCreated(arguments.dateCreated) />
        <cfreturn this />
    </cffunction>

</cfcomponent>
