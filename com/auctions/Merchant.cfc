<cfcomponent accessors="1">

    <!--- properties //////////////////////////////////--->
    <cfproperty name="id" type="numeric" />
    <cfproperty name="firstName" type="string" />
    <cfproperty name="middleName" type="string" />
    <cfproperty name="lastName" type="string" />
    <cfproperty name="fullName" type="string" />
    <cfproperty name="email" type="string" />
    <cfproperty name="phone1" type="string" />
    <cfproperty name="phone2" type="string" />
    <cfproperty name="phone3" type="string" />
    <cfproperty name="countryID" type="numeric" />
    <cfproperty name="dateCreated" type="date" />
    <cfproperty name="adminID" type="numeric" />

    <!--- public methods //////////////////////////////--->

    <!--- function: init --------------------------------->
    <cffunction name="init" returntype="Merchant">
        <cfargument name="id" type="numeric" required="1" />
        <cfargument name="firstName" type="string" required="1" />
        <cfargument name="middleName" type="string" required="0"default="" />
        <cfargument name="lastName" type="string" required="1" />
        <cfargument name="email" type="string" required="1" />
        <cfargument name="phone1" type="string" required="0" default="" />
        <cfargument name="phone2" type="string" required="0" default="" />
        <cfargument name="phone3" type="string" required="0" default="" />
        <cfargument name="countryID" type="numeric" required="1" />
        <cfargument name="dateCreated" type="date" required="1" />
        <cfargument name="adminID" type="numeric" required="1" />
        <cfset setID(arguments.id) />
        <cfset setFirstName(arguments.firstName) />
        <cfset setMiddleName(arguments.middleName) />
        <cfset setLastName(arguments.lastName) />
        <cfset setFullName(arguments.firstName & " " & arguments.lastName) />
        <cfset setEmail(arguments.email) />
        <cfset setPhone1(arguments.phone1) />
        <cfset setPhone2(arguments.phone2) />
        <cfset setPhone3(arguments.phone3) />
        <cfset setCountryID(arguments.countryID) />
        <cfset setDateCreated(arguments.dateCreated) />
        <cfset setAdminID(arguments.adminID) />
        <cfreturn this />
    </cffunction>

</cfcomponent>