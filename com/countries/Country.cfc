<cfcomponent accessors="1">

<!--- properties //////////////////////////////////--->
    <cfproperty name="id" type="numeric" />
    <cfproperty name="iso" type="string" />
    <cfproperty name="name" type="string" />
    <cfproperty name="niceName" type="string" />
    <cfproperty name="iso3" type="string" />
    <cfproperty name="numCode" type="string" />
    <cfproperty name="phoneCode" type="string" />

<!--- public methods //////////////////////////////--->

<!--- function: init --------------------------------->
    <cffunction name="init" returntype="Country">
        <cfargument type="numeric" required="1" name="id" />
        <cfargument type="string" required="1" name="iso" />
        <cfargument type="string" required="1" name="name" />
        <cfargument type="string" required="1" name="nicename" />
        <cfargument type="string" required="1" name="iso3" />
        <cfargument type="string" required="1" name="numCode" />
        <cfargument type="string" required="1" name="phoneCode" />
        <cfset setID(arguments.id) />
        <cfset setISO(arguments.iso) />
        <cfset setName(arguments.name) />
        <cfset setNiceName(arguments.niceName) />
        <cfset setISO3(arguments.iso3) />
        <cfset setNumCode(arguments.numCode) />
        <cfset setPhoneCode(arguments.phoneCode) />
        <cfreturn this />
    </cffunction>

</cfcomponent>