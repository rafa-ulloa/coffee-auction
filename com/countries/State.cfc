<cfcomponent accessors="1">

<!--- properties //////////////////////////////////--->
    <cfproperty name="id" type="numeric" />
    <cfproperty name="countryID" type="numeric" />
    <cfproperty name="name" type="string" />
    <cfproperty name="abbreviation" type="string" />

<!--- public methods //////////////////////////////--->

<!--- function: init --------------------------------->
    <cffunction name="init" returntype="State">
        <cfargument type="numeric" required="1" name="id" />
        <cfargument type="numeric" required="1" name="countryID" />
        <cfargument type="string" required="1" name="name" />
        <cfargument type="string" required="1" name="abbreviation" />
        <cfset setID(arguments.id) />
        <cfset setCountryID(arguments.countryID) />
        <cfset setName(arguments.name) />
        <cfset setAbbreviation(arguments.abbreviation) />
        <cfreturn this />
    </cffunction>

</cfcomponent>