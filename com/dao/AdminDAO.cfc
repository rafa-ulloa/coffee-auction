<cfcomponent accessors="1">

    <!--- properties //////////////////////////////////--->
    <cfproperty type="string" name="datasource" />

    <!--- public methods///////////////////////////////--->

    <!--- function: init---------------------------------->
    <cffunction name="init" returntype="AdminDAO">
        <cfargument type="string" required="1" name="datasource" />
        <cfset setDatasource(arguments.datasource) />
        <cfreturn this />
    </cffunction>

    <!--- function: updateAdmin--------------------------->
    <cffunction name="updateAdmin" returntype="void" description="updates an admin or creates a new one if not found">
        <cfargument name="id" type="numeric" required="0" default="0" />
        <cfargument name="username" type="string" required="1" />
        <cfargument name="password" type="string" required="1" />
        <cfargument name="email" type="string" required="1" />

        <!--- insert record to database --->
        <cfquery datasource="#getDatasource()#">
            INSERT INTO admins
            VALUES(
                <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.id#" />,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.username#" />,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#hash(arguments.password)#" />,
                NOW(),
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.email#" />
            )
            ON DUPLICATE KEY UPDATE
                email = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.email#" />
        </cfquery>
    </cffunction>

    <!--- function: searchAdmins-------------------------->
    <cffunction name="searchAdmins" returntype="query">
        <cfargument type="numeric" required="0" name="id" default="0" />
        <cfargument type="string" required="0" name="username" default="" />
        <cfargument type="string" required="0" name="email" default="" />
        <cfargument type="numeric" required="0" name="blockFactor" default="0" hint="max records in data block (query) retrieved" />
        <cfargument type="numeric" required="0" name="blockOffset" default="0" hint="amount of data blocks to offset using the same block factor (for pagination)" />
        <cfset qAdmins = queryNew("") />
        <cfquery name="qAdmins" datasource="#getDatasource()#">
            SELECT
                *
            FROM
                admins
            WHERE
                1=1
                <cfif arguments.id>AND id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.id#" /></cfif>
                <cfif len(arguments.username)>AND username LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.username#%" /></cfif>
                <cfif len(arguments.email)>AND email = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.email#" /></cfif>
                <cfif arguments.blockFactor>LIMIT #arguments.blockFactor * arguments.blockOffset#, #arguments.blockFactor#</cfif>
        </cfquery>
        <cfreturn qAdmins />
    </cffunction>

    <!--- function:getRecordCount------------------------->
    <cffunction name="getRecordCount" returntype="numeric" description="gets user record count. used for search page pagination.">
        <cfargument type="numeric" required="0" name="userID" default="0" />
        <cfargument type="string" required="0" name="username" default="" />
        <cfargument type="string" required="0" name="email" default="" />
        <cfset var qRecordCount = queryNew("") />
        <cfquery name="qRecordCount" datasource="#getDatasource()#">
            SELECT
                COUNT(*) as counts
            FROM
                admins
            WHERE
                1 = 1
                <cfif arguments.id>AND id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.id#" /></cfif>
                <cfif len(arguments.username)>AND username LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.username#%" /></cfif>
                <cfif len(arguments.email)>AND email = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.email#" /></cfif>
        </cfquery>
        <cfreturn qRecordCount.counts />
    </cffunction>

    <!--- function: deleteAdmin--------------------------->
    <cffunction name="deleteAdmin" returntype="void">
        <cfargument type="numeric" required="1" name="id" />
        <cfquery datasource="#getDatasource()#">
            DELETE FROM
                admins
            WHERE
                id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.id#" />
        </cfquery>
    </cffunction>

    <!--- function: getAdminByLogin----------------------->
    <cffunction name="getAdminByLogin" returntype="query">
        <cfargument type="string" required="1" name="username" />
        <cfargument type="string" required="1" name="password" />
        <cfset var qAdmin = queryNew("") />
        <cfquery name="qAdmin" datasource="#getDatasource()#">
            SELECT
                *
            FROM
                admins
            WHERE
                username = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.username#" /> AND
                password = <cfqueryparam cfsqltype="cf_sql_varchar" value="#hash(arguments.password)#" />
        </cfquery>
        <cfreturn qAdmin />
    </cffunction>

</cfcomponent>
