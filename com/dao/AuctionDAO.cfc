<cfcomponent accessors="1">

    <!--- properties //////////////////////////////////--->
    <cfproperty type="string" name="datasource" />

    <!--- public methods///////////////////////////////--->

    <!--- function: init---------------------------------->
    <cffunction name="init" returntype="AuctionDAO">
        <cfargument type="string" required="1" name="datasource" />
        <cfset setDatasource(arguments.datasource) />
        <cfreturn this />
    </cffunction>

    <!--- function: updateAuction---------------------------->
    <cffunction name="updateAuction" returntype="void" description="updates an auction or creates a new one if not found">
        <cfargument name="id" type="numeric" required="1" />
        <cfargument name="startDate" type="date" required="1" />
        <cfargument name="endDate" type="date" required="1" />
        <cfargument name="merchantID" type="numeric" required="1" />
        <cfargument name="adminID" type="numeric" required="1" />
        <cfargument name="description" type="string" required="1" />
	<cfargument name="startingPrice" type="numeric" required="1" />
	<cfargument name="isPaid" type="boolean" required="0" default="0" />
	<cfargument name="paymentMethodID" type="numeric" required="0" default="0" />
	<cfargument name="paymentConfirmationNumber" type="string" required="0" default="" />

        <!--- insert record to database --->
        <cfquery datasource="#getDatasource()#">
            INSERT INTO auctions
            VALUES(
                <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.id#" />,
                <cfqueryparam cfsqltype="cf_sql_date" value="#arguments.startDate#" />,
                <cfqueryparam cfsqltype="cf_sql_date" value="#arguments.endDate#" />,
                <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.merchantID#" />,
                NOW(),
                <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.adminID#" />,
                <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#arguments.description#" />,
		<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.startingPrice#" />,
		<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.isPaid ? 1 : 0#" />,
		<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.paymentMethodID#" null="#!arguments.paymentMethodID#" />,
		<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.paymentConfirmationNumber#" null="#!len(arguments.paymentConfirmationNumber)#" />
            )
            ON DUPLICATE KEY UPDATE
                startDate = <cfqueryparam cfsqltype="cf_sql_date" value="#arguments.startDate#" />,
                endDate = <cfqueryparam cfsqltype="cf_sql_date" value="#arguments.endDate#" />,
                merchantID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.merchantID#" />,
                description = <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#arguments.description#" />,
                startingPrice = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.startingPrice#" />,
		isPaid = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.isPaid ? 1 : 0#" />,
		paymentMethodID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.paymentMethodID#" null="#!arguments.paymentMethodID#" />,
		paymentConfirmationNumber = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.paymentConfirmationNumber#" null="#!len(arguments.paymentConfirmationNumber)#" />
        </cfquery>

    </cffunction>

    <!--- function: searchAuctions--------------------------->
    <cffunction name="searchAuctions" returntype="query">
        <cfargument name="id" type="numeric" required="0" default="0" />
        <cfargument name="startDate" type="date" required="0" />
        <cfargument name="endDate" type="date" required="0" />
        <cfargument name="merchantID" type="numeric" required="0" default="0" />
        <cfargument name="merchantName" type="string" required="0" default="" />
        <cfargument name="dateCreated" type="date" required="0" />
        <cfargument name="adminID" type="numeric" required="0" default="0" />
        <cfargument type="numeric" required="0" name="blockFactor" default="0" hint="max records in data block (query) retrieved" />
        <cfargument type="numeric" required="0" name="blockOffset" default="0" hint="amount of data blocks to offset using the same block factor (for pagination)" />
        <cfset qUsers = queryNew("") />
        <cfquery name="qUsers" datasource="#getDatasource()#">
            SELECT
                *
            FROM
                auctions
            WHERE
                1=1
                <cfif arguments.id>AND id = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.id#" /></cfif>
                <cfif !isNull(arguments.startDate)>AND startDate <= <cfqueryparam cfsqltype="cf_sql_date" value="#arguments.startDate#" /></cfif>
                <cfif !isNull(arguments.endDate)>AND endDate >= <cfqueryparam cfsqltype="cf_sql_date" value="#arguments.endDate#" /></cfif>
                <cfif arguments.merchantID>AND merchantID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.merchantID#" /></cfif>
                <cfif len(arguments.merchantName)>AND merchantName LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.merchantName#%" /></cfif>
                <cfif !isNull(arguments.dateCreated)>AND dateCreated = <cfqueryparam cfsqltype="cf_sql_date" value="#arguments.dateCreated#" /></cfif>
                <cfif arguments.adminID>AND adminID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.adminID#" /></cfif>
                <cfif arguments.blockFactor>LIMIT #arguments.blockFactor * arguments.blockOffset#, #arguments.blockFactor#</cfif>
        </cfquery>
        <cfreturn qUsers />
    </cffunction>

    <!---function:getAuctionsByUserID--------------------->
    <cffunction name="getAuctionsByUserID" returntype="query">
    	<cfargument name="id" type="numeric" required="1" />
	<cfset var qAuctions = queryNew("") />
	<cfquery name="qAuctions" datasource="#getDatasource()#">
		SELECT
			a.*
		FROM
			auctions a
		WHERE
			EXISTS (
				SELECT
					1
				FROM
					bids b
				WHERE
					b.auctionID = a.id
				AND
					b.userID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.id#" />
			)
	</cfquery>
	<cfreturn qAuctions />
    </cffunction>

    <!--- function:getAuctionsWithEndDate----------------->
    <cffunction name="getAuctionsWithEndDate" returntype="query" description="gets auctions with the EXACT end date provided (userful for closing auctions)">
    	<cfargument name="endDate" type="date" required="1" />
	<cfset var qAuctions = queryNew("") />
	<cfquery name="qAuctions" datasource="#getDatasource()#">
		SELECT
			*
		FROM
			auctions
		WHERE
			endDate = <cfqueryparam cfsqltype="cf_sql_date" value="#arguments.endDate#" />
	</cfquery>
	<cfreturn qAuctions />
    </cffunction>

    <!--- function:getAuctionWinners---------------------->
    <cffunction name="getAuctionWinners" returntype="query"description="gets the winners for the provided auction IDs">
    	<cfargument name="idList" type="string" required="1" hint="list of auction IDs" />
	<cfset var qUsers = queryNew("") />
	<cfquery name="qUsers" datasource="#getDatasource()#">
		SELECT
			a.userID, MAX(a.amount) as amount, a.auctionID, b.email
		FROM
			bids a, users b
		WHERE
			auctionID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.idList#" null="#!len(arguments.idList)#" />)
		AND
			a.userID = b.id
		GROUP BY
			auctionID
		
	</cfquery>
	<cfreturn qUsers />
    </cffunction>

    <!--- function:getRecordCount------------------------->
    <cffunction name="getRecordCount" returntype="numeric" description="gets user record count. used for search page pagination.">
        <cfargument name="id" type="numeric" required="0" default="0" />
        <cfargument name="startDate" type="date" required="0" />
        <cfargument name="endDate" type="date" required="0" />
        <cfargument name="merchantID" type="numeric" required="0" default="0" />
        <cfargument name="merchantName" type="string" required="0" default="" />
        <cfargument name="dateCreated" type="date" required="0" />
        <cfargument name="adminID" type="numeric" required="0" default="0" />
        <cfset var qRecordCount = queryNew("") />
        <cfquery name="qRecordCount" datasource="#getDatasource()#">
            SELECT
                COUNT(*) as counts
            FROM
                auctions
            WHERE
                1 = 1
                <cfif arguments.id>AND id = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.id#" /></cfif>
                <cfif !isNull(arguments.startDate)>AND startDate = <cfqueryparam cfsqltype="cf_sql_date" value="#arguments.startDate#" /></cfif>
                <cfif !isNull(arguments.endDate)>AND endDate = <cfqueryparam cfsqltype="cf_sql_date" value="#arguments.endDate#" /></cfif>
                <cfif arguments.merchantID>AND merchantID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.merchantID#" /></cfif>
                <cfif len(arguments.merchantName)>AND merchantName LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.merchantName#%" /></cfif>
                <cfif !isNull(arguments.dateCreated)>AND dateCreated = <cfqueryparam cfsqltype="cf_sql_date" value="#arguments.dateCreated#" /></cfif>
                <cfif arguments.adminID>AND adminID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.adminID#" /></cfif>
        </cfquery>
        <cfreturn qRecordCount.counts />
    </cffunction>

    <!--- function: deleteAuction---------------------------->
    <cffunction name="deleteUser" returntype="void">
        <cfargument type="numeric" required="1" name="id" />
        <cfquery datasource="#getDatasource()#">
            DELETE FROM
                auctions
            WHERE
                id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.id#" />
        </cfquery>
    </cffunction>

    <!--- function: getAuctionHighestBid------------------>
    <cffunction name="getAuctionHighestBid" returntype="query">
        <cfargument type="numeric" required="1" name="id" />
        <cfset var qBid = queryNew("") />
        <cfquery name="qBid" datasource="#getDatasource()#">
            SELECT
                *
            FROM
                bids
            WHERE
                auctionID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.id#" />
            ORDER BY
                amount DESC
            LIMIT
                1
        </cfquery>
        <cfreturn qBid />
    </cffunction>

    <!---function:setAuctionAsPaid------------------------>
    <cffunction name="setAuctionAsPaid" returntype="void">
    	<cfargument name="id" type="numeric" required="1" hint="auction ID" />
    	<cfargument name="paymentMethodID" type="numeric" required="1" hint="paypal, visa, master card, etc." />
	<cfargument name="paymentConfirmationNumber" type="string" required="1" />
	<cfquery datasource="#getDatasource()#">
		UPDATE
			auctions
		SET
			isPaid = 1,
			paymentMethodID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.paymentMethodID#" />,
			paymentConfirmationNumber = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.paymentConfirmationNumber#" null="#!len(arguments.paymentConfirmationNumber)#" />
		WHERE
			id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.id#" />
	</cfquery>
    </cffunction>

</cfcomponent>
