<cfcomponent accessors="1">

    <!--- properties //////////////////////////////////--->
    <cfproperty type="string" name="datasource" />

    <!--- public methods///////////////////////////////--->

    <!--- function: init---------------------------------->
    <cffunction name="init" returntype="BidDAO">
        <cfargument type="string" required="1" name="datasource" />
        <cfset setDatasource(arguments.datasource) />
        <cfreturn this />
    </cffunction>

    <!--- function: updateBid----------------------------->
    <cffunction name="updateBid" returntype="void" description="updates a bid or creates a new one if not found">
        <cfargument name="id" type="numeric" required="0" default="0" />
        <cfargument name="auctionID" type="numeric" required="1" />
        <cfargument name="userID" type="numeric" required="1" />
        <cfargument name="amount" type="numeric" required="1" />

        <!--- insert record to database --->
        <cfquery datasource="#getDatasource()#">
            INSERT INTO bids
            VALUES(
                <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.id#" />,
                <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.auctionID#" />,
                <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.userID#" />,
                <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.amount#" />,
		NOW()
            )
            ON DUPLICATE KEY UPDATE
                auctionID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.auctionID#" />,
                userID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.userID#" />,
                amount = <cfqueryparam cfsqltype="cf_sql_decimal" value="#arguments.amount#" />
        </cfquery>
    </cffunction>

    <!--- function: searchBids--------------------------->
    <cffunction name="searchBids" returntype="query">
        <cfargument name="id" type="numeric" required="0" default="0" />
        <cfargument name="auctionID" type="numeric" required="0" default="0" />
        <cfargument name="userID" type="numeric" required="0" default="0" />
        <cfargument name="amount" type="numeric" required="0" default="0" />
        <cfargument type="numeric" required="0" name="blockFactor" default="0" hint="max records in data block (query) retrieved" />
        <cfargument type="numeric" required="0" name="blockOffset" default="0" hint="amount of data blocks to offset using the same block factor (for pagination)" />
        <cfset var qBids = queryNew("") />
        <cfquery name="qBids" datasource="#getDatasource()#">
            SELECT
                *
            FROM
                bids
            WHERE
                1=1
		<cfif arguments.id>AND id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.userID#" /></cfif>
                <cfif arguments.auctionID>AND auctionID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.auctionID#" /></cfif>
                <cfif arguments.userID>AND userID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.userID#" /></cfif>
		<cfif arguments.amount>AND amount = <cfqueryparam cfsqltype="cf_sql_decimal" value="#arguments.amount#" /></cfif>
                <cfif arguments.blockFactor>LIMIT #arguments.blockFactor * arguments.blockOffset#, #arguments.blockFactor#</cfif>
        </cfquery>
        <cfreturn qBids />
    </cffunction>

    <!--- function:getRecordCount------------------------->
    <cffunction name="getRecordCount" returntype="numeric" description="gets bid record count. used for search page pagination.">
        <cfargument name="id" type="numeric" required="0" default="0" />
        <cfargument name="auctionID" type="numeric" required="1" />
        <cfargument name="userID" type="numeric" required="1" />
        <cfargument name="amount" type="numeric" required="1" />
        <cfset var qRecordCount = queryNew("") />
        <cfquery name="qRecordCount" datasource="#getDatasource()#">
            SELECT
                COUNT(*) as counts
            FROM
                bids
            WHERE
                1 = 1
		<cfif arguments.id>AND id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.userID#" /></cfif>
                <cfif arguments.auctionID>AND auctionID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.auctionID#" /></cfif>
                <cfif arguments.userID>AND userID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.userID#" /></cfif>
                <cfif arguments.amount>AND amount = <cfqueryparam cfsqltype="cf_sql_decimal" value="#arguments.amount#" /></cfif>
        </cfquery>
        <cfreturn qRecordCount.counts />
    </cffunction>

    <!--- function: deleteUser---------------------------->
    <cffunction name="deleteBid" returntype="void">
        <cfargument type="numeric" required="1" name="id" />
        <cfquery datasource="#getDatasource()#">
            DELETE FROM
                bids
            WHERE
                id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.id#" />
        </cfquery>
    </cffunction>

</cfcomponent>
