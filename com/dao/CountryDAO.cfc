<cfcomponent accessors="1">

    <!--- properties //////////////////////////////////--->
    <cfproperty type="string" name="datasource" />

    <!--- public methods///////////////////////////////--->

    <!--- function: init---------------------------------->
    <cffunction name="init" returntype="CountryDAO">
        <cfargument type="string" required="1" name="datasource" />
        <cfset setDatasource(arguments.datasource) />
        <cfreturn this />
    </cffunction>

    <!--- function: searchUsers--------------------------->
    <cffunction name="getCountries" returntype="query">
        <cfset qCountries = queryNew("") />
        <cfquery name="qCountries" datasource="#getDatasource()#">
            SELECT
                *
            FROM
                countries
            ORDER BY
                nicename
        </cfquery>
        <cfreturn qCountries />
    </cffunction>

</cfcomponent>
