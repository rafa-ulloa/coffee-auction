<cfcomponent accessors="1">

    <!--- properties //////////////////////////////////--->
    <cfproperty type="string" name="datasource" />

    <!--- public methods///////////////////////////////--->

    <!--- function: init---------------------------------->
    <cffunction name="init" returntype="MerchantDAO">
        <cfargument type="string" required="1" name="datasource" />
        <cfset setDatasource(arguments.datasource) />
        <cfreturn this />
    </cffunction>

    <!--- function: updateMerchant------------------------>
    <cffunction name="updateMerchant" returntype="void" description="updates a merchant or creates a new one if not found">
        <cfargument name="id" type="numeric" required="1" />
        <cfargument name="firstName" type="string" required="1" />
        <cfargument name="middleName" type="string" required="0"default="" />
        <cfargument name="lastName" type="string" required="1" />
        <cfargument name="email" type="string" required="0" default="" />
        <cfargument name="phone1" type="string" required="0" default="" />
        <cfargument name="phone2" type="string" required="0" default="" />
        <cfargument name="phone3" type="string" required="0" default="" />
        <cfargument name="countryID" type="numeric" required="1" />
        <cfargument name="adminID" type="numeric" required="1" />

        <!--- insert record to database --->
        <cfquery datasource="#getDatasource()#">
            INSERT INTO merchants
            VALUES(
                <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.id#" />,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.firstName#" />,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.middleName#" null="#!len(arguments.middleName)#" />,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.lastName#" />,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.email#" null="#!len(arguments.email)#" />,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.phone1#" null="#!len(arguments.phone1)#" />,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.phone2#" null="#!len(arguments.phone2)#" />,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.phone3#" null="#!len(arguments.phone3)#" />,
                <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.countryID#" />,
                NOW(),
                <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.adminID#" />
            )
            ON DUPLICATE KEY UPDATE
                firstName = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.firstName#" />,
                middleName = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.middleName#" null="#!len(arguments.middleName)#" />,
                lastName = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.lastName#" />,
                email = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.email#" null="#!len(arguments.email)#" />,
                phone1 = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.phone1#" null="#!len(arguments.phone1)#" />,
                phone2 = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.phone2#" null="#!len(arguments.phone2)#" />,
                phone3 = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.phone3#" null="#!len(arguments.phone3)#" />
        </cfquery>
    </cffunction>

    <!--- function: searchMerchants--------------------------->
    <cffunction name="searchMerchants" returntype="query">
        <cfargument name="id" type="numeric" required="0" default="0" />
        <cfargument name="firstName" type="string" required="0" />
        <cfargument name="middleName" type="string" required="0" />
        <cfargument name="lastName" type="string" required="0" />
        <cfargument name="email" type="string" required="0" />
        <cfargument name="phone" type="string" required="0" />
        <cfargument name="countryID" type="numeric" required="0" default="0" />
        <cfargument name="adminID" type="numeric" required="0" default="0" />
        <cfargument type="numeric" required="0" name="blockFactor" default="0" hint="max records in data block (query) retrieved" />
        <cfargument type="numeric" required="0" name="blockOffset" default="0" hint="amount of data blocks to offset using the same block factor (for pagination)" />
        <cfset qMerchants = queryNew("") />
        <cfquery name="qMerchants" datasource="#getDatasource()#">
            SELECT
                *
            FROM
                merchants
            WHERE
                1=1
                <cfif arguments.id>AND id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.id#" /></cfif>
                <cfif !isNull(arguments.firstName)>AND firstName LIKE <cfqueryparam cfsqltype="cf_sql_integer" value="%#arguments.firstName#%" /></cfif>
                <cfif !isNull(arguments.middleName)>AND middleName LIKE <cfqueryparam cfsqltype="cf_sql_integer" value="%#arguments.middleName#%" /></cfif>
                <cfif !isNull(arguments.lastName)>AND lastName LIKE <cfqueryparam cfsqltype="cf_sql_integer" value="%#arguments.lastName#%" /></cfif>
                <cfif !isNull(arguments.email)>AND email = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.email#" /></cfif>
                <cfif !isNull(arguments.phone)>AND <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.phone#" /> IN (phone1,phone2,phone3)</cfif>
                <cfif arguments.countryID>AND countryID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.countryID#" /></cfif>
                <cfif arguments.adminID>AND adminID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.adminID#" /></cfif>
                <cfif arguments.blockFactor>LIMIT #arguments.blockFactor * arguments.blockOffset#, #arguments.blockFactor#</cfif>
        </cfquery>
        <cfreturn qMerchants />
    </cffunction>

    <!--- function:getRecordCount------------------------->
    <cffunction name="getRecordCount" returntype="numeric" description="gets merchant record count. used for search page pagination.">
        <cfargument name="id" type="numeric" required="0" default="0" />
        <cfargument name="firstName" type="string" required="0" />
        <cfargument name="middleName" type="string" required="0" />
        <cfargument name="lastName" type="string" required="0" />
        <cfargument name="email" type="string" required="0" />
        <cfargument name="phone" type="string" required="0" />
        <cfargument name="countryID" type="numeric" required="0" default="0" />
        <cfargument name="adminID" type="numeric" required="0" default="0" />
        <cfset var qRecordCount = queryNew("") />
        <cfquery name="qRecordCount" datasource="#getDatasource()#">
            SELECT
                COUNT(*) as counts
            FROM
                merchants
            WHERE
                1 = 1
                <cfif arguments.id>AND id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.id#" /></cfif>
                <cfif !isNull(arguments.firstName)>AND firstName LIKE <cfqueryparam cfsqltype="cf_sql_integer" value="%#arguments.firstName#%" /></cfif>
                <cfif !isNull(arguments.middleName)>AND middleName LIKE <cfqueryparam cfsqltype="cf_sql_integer" value="%#arguments.middleName#%" /></cfif>
                <cfif !isNull(arguments.lastName)>AND lastName LIKE <cfqueryparam cfsqltype="cf_sql_integer" value="%#arguments.lastName#%" /></cfif>
                <cfif !isNull(arguments.email)>AND email = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.email#" /></cfif>
                <cfif !isNull(arguments.phone)>AND <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.phone#" /> IN (phone1,phone2,phone3)</cfif>
                <cfif arguments.countryID>AND countryID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.countryID#" /></cfif>
                <cfif arguments.adminID>AND adminID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.adminID#" /></cfif>
                <cfif arguments.blockFactor>LIMIT #arguments.blockFactor * arguments.blockOffset#, #arguments.blockFactor#</cfif>
        </cfquery>
        <cfreturn qRecordCount.counts />
    </cffunction>

    <!--- function: deleteMerchant---------------------------->
    <cffunction name="deleteMerchant" returntype="void">
        <cfargument type="numeric" required="1" name="id" />
        <cfquery datasource="#getDatasource()#">
            DELETE FROM
                merchants
            WHERE
                id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.id#" />
        </cfquery>
    </cffunction>

</cfcomponent>
