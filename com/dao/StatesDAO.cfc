<cfcomponent accessors="1">

    <!--- properties //////////////////////////////////--->
    <cfproperty type="string" name="datasource" />

    <!--- public methods///////////////////////////////--->

    <!--- function: init---------------------------------->
    <cffunction name="init" returntype="StatesDAO">
        <cfargument type="string" required="1" name="datasource" />
        <cfset setDatasource(arguments.datasource) />
        <cfreturn this />
    </cffunction>

    <!--- function: getStates--------------------------->
    <cffunction name="getStates" returntype="query">
        <cfset var qStates = queryNew("") />
        <cfquery name="qStates" datasource="#getDatasource()#">
            SELECT
                *
            FROM
                states
            ORDER BY
                name
        </cfquery>
        <cfreturn qStates />
    </cffunction>

    <!--- function: getStateByID-------------------------->
    <cffunction name="getStateByID" returntype="query">
        <cfargument type="numeric" required="1" name="id" />
        <cfset var qState = queryNew("")>
        <cfquery name="qState" datasource="#getDatasource()#">
            SELECT
                *
            FROM
                states
            WHERE
                id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.id#" />
        </cfquery>
        <cfreturn qState />
    </cffunction>

</cfcomponent>
