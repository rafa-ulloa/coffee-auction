<cfcomponent accessors="1">

    <!--- properties //////////////////////////////////--->
    <cfproperty type="string" name="datasource" />

    <!--- public methods///////////////////////////////--->

    <!--- function: init---------------------------------->
    <cffunction name="init" returntype="UserDAO">
        <cfargument type="string" required="1" name="datasource" />
        <cfset setDatasource(arguments.datasource) />
        <cfreturn this />
    </cffunction>

    <!--- function: updateUser---------------------------->
    <cffunction name="updateUser" returntype="void" description="updates a user or creates a new one if not found">
        <cfargument name="userID" type="numeric" required="0" default="0" />
        <cfargument name="username" type="string" required="1" />
        <cfargument name="password" type="string" required="1" />
        <cfargument name="email" type="string" required="1" />
        <cfargument name="countryID" type="numeric" required="1"     />
        <cfargument name="adminID" type="string" required="1" hint="for auditing purposes, admin who created user" />
        <cfargument name="shippingAddress_line1" type="string" required="1" />
        <cfargument name="shippingAddress_line2" type="string" required="0" default="" />
        <cfargument name="state" type="string" required="0" default="" />
        <cfargument name="city" type="string" required="0" default="" />
        <cfargument name="postalCode" type="string" required="0" default="" />

        <!--- insert record to database --->
        <cfquery datasource="#getDatasource()#">
            INSERT INTO users
            VALUES(
                <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.userID#" />,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.username#" />,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#hash(arguments.password)#" />,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.email#" />,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.shippingAddress_line1#" />,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#len(arguments.shippingAddress_line2)#" null="#!len(arguments.shippingAddress_line2)#" />,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.state#" />,
                <cfqueryparam cfsqltype="cf_sql_integer" value="#val(arguments.countryID)#" />,
                NOW(),
                <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.adminID#" />,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.city#" />,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.postalCode#" null="#!len(arguments.postalCode)#" />
            )
            ON DUPLICATE KEY UPDATE
                email = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.email#" />,
                countryID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.countryID#" />,
                shippingAddress_line1 = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.shippingAddress_line1#" />,
                shippingAddress_line2 = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.shippingAddress_line2#" null="#!len(arguments.shippingAddress_line2)#" />,
                state = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.state#" />,
		city = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.city#" />
        </cfquery>
    </cffunction>

    <!--- function: changePassword------------------------>
    <cffunction name="changePassword" returntype="void" description="updates a user or creates a new one if not found">
        <cfargument name="userID" type="numeric" required="0" default="0" />
        <cfargument name="password" type="string" required="1" />

        <!--- insert record to database --->
        <cfquery datasource="#getDatasource()#">
            UPDATE
                users
            SET
                password = <cfqueryparam cfsqltype="cf_sql_varchar" value="#hash(arguments.password)#" />
            WHERE
                id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.userID#" />
        </cfquery>
    </cffunction>

    <!--- function: searchUsers--------------------------->
    <cffunction name="searchUsers" returntype="query">
        <cfargument type="numeric" required="0" name="userID" default="0" />
        <cfargument type="string" required="0" name="username" default="" />
        <cfargument type="string" required="0" name="email" default="" />
        <cfargument type="numeric" required="0" name="countryID" default="0" />
        <cfargument type="numeric" required="0" name="adminID" default="0" />
        <cfargument type="numeric" required="0" name="blockFactor" default="0" hint="max records in data block (query) retrieved" />
        <cfargument type="numeric" required="0" name="blockOffset" default="0" hint="amount of data blocks to offset using the same block factor (for pagination)" />
        <cfset qUsers = queryNew("") />
        <cfquery name="qUsers" datasource="#getDatasource()#">
            SELECT
                *
            FROM
                users
            WHERE
                1=1
                <cfif userID>AND id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.userID#" /></cfif>
                <cfif len(username)>AND username LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.username#%" /></cfif>
                <cfif len(email)>AND email = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.email#" /></cfif>
                <cfif countryID>AND countryID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.countryID#" /></cfif>
                <cfif adminID>AND adminID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.adminID#" /></cfif>
                <cfif arguments.blockFactor>LIMIT #arguments.blockFactor * arguments.blockOffset#, #arguments.blockFactor#</cfif>
        </cfquery>
        <cfreturn qUsers />
    </cffunction>

    <!--- function:getRecordCount------------------------->
    <cffunction name="getRecordCount" returntype="numeric" description="gets user record count. used for search page pagination.">
        <cfargument type="numeric" required="0" name="userID" default="0" />
        <cfargument type="string" required="0" name="username" default="" />
        <cfargument type="string" required="0" name="email" default="" />
        <cfargument type="numeric" required="0" name="countryID" default="0" />
        <cfargument type="numeric" required="0" name="adminID" default="0" />
        <cfset var qRecordCount = queryNew("") />
        <cfquery name="qRecordCount" datasource="#getDatasource()#">
            SELECT
                COUNT(*) as counts
            FROM
                users
            WHERE
                1 = 1
                <cfif userID>AND userID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.userID#" /></cfif>
                <cfif len(username)>AND username LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.username#%" /></cfif>
                <cfif len(email)>AND email = <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.email#%" /></cfif>
                <cfif countryID>AND countryID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.countryID#" /></cfif>
                <cfif adminID>AND adminID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.adminID#" /></cfif>
        </cfquery>
        <cfreturn qRecordCount.counts />
    </cffunction>

    <!--- function: deleteUser---------------------------->
    <cffunction name="deleteUser" returntype="void">
        <cfargument type="numeric" required="1" name="userID" />
        <cfquery datasource="#getDatasource()#">
            DELETE FROM
                users
            WHERE
                userID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.userID#" />
        </cfquery>
    </cffunction>

    <!--- function: getUserByLogin------------------------>
    <cffunction name="getUserByLogin" returntype="query">
        <cfargument type="string" required="1" name="username" />
        <cfargument type="string" required="1" name="password" />
        <cfset var qUser = queryNew("") />
        <cfquery name="qUser" datasource="#getDatasource()#">
            SELECT
                *
            FROM
                users
            WHERE
                username = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.username#" /> AND
                password = <cfqueryparam cfsqltype="cf_sql_varchar" value="#hash(arguments.password)#" />
        </cfquery>
        <cfreturn qUser />
    </cffunction>

</cfcomponent>
