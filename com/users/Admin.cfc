<cfcomponent accessors="1">

    <!--- properties //////////////////////////////////--->
    <cfproperty name="id" type="numeric" />
    <cfproperty name="username" type="string" />
    <cfproperty name="email" type="string" />
    <cfproperty name="dateCreated" type="date" />

    <!--- public methods //////////////////////////////--->

    <!--- function: init --------------------------------->
    <cffunction name="init" returntype="Admin">
        <cfargument type="numeric" required="1" name="id" />
        <cfargument type="string" required="1" name="username" />
        <cfargument type="string" required="1" name="email" />
        <cfargument type="date" required="1" name="dateCreated" />
        <cfset setID(arguments.id) />
        <cfset setUsername(arguments.username) />
        <cfset setEmail(arguments.email) />
        <cfset setDateCreated(arguments.dateCreated) />
        <cfreturn this />
    </cffunction>

</cfcomponent>
