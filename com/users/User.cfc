<cfcomponent accessors="1">

    <!--- properties //////////////////////////////////--->
    <cfproperty name="id" type="numeric" />
    <cfproperty name="username" type="string" />
    <cfproperty name="email" type="string" />
    <cfproperty name="state" type="string" />
    <cfproperty name="city" type="string" />
    <cfproperty name="shippingAddressLine1" type="string" />
    <cfproperty name="shippingAddressLine2" type="string" />
    <cfproperty name="countryID" type="numeric" />
    <cfproperty name="dateCreated" type="date" />
    <cfproperty name="adminID" type="numeric" />

    <!--- public methods //////////////////////////////--->

    <!--- function: init --------------------------------->
    <cffunction name="init" returntype="User">
        <cfargument type="numeric" required="0" name="id" default="0" />
        <cfargument type="string" required="0" name="username" default="" />
        <cfargument type="string" required="0" name="email" default="" />
        <cfargument type="string" required="0" name="state" default="" />
        <cfargument type="string" required="0" name="city" default="" />
        <cfargument type="string" required="0" name="shippingAddressLine1" default="" />
        <cfargument type="string" required="0" name="shippingAddressLine2" default="" />
        <cfargument type="numeric" required="0" name="countryID" default="0" />
        <cfargument type="date" required="0" name="dateCreated" default="#now()#" />
        <cfargument type="numeric" required="0" name="adminID" default="0" />
        <cfset setID(arguments.id) />
        <cfset setUsername(arguments.username) />
        <cfset setEmail(arguments.email) />
        <cfset setState(arguments.state) />
        <cfset setCity(arguments.city) />
        <cfset setShippingAddressLine1(arguments.shippingAddressLine1) />
        <cfset setShippingAddressLine2(arguments.shippingAddressLine2) />
        <cfset setCountryID(arguments.countryID) />
        <cfset setDateCreated(arguments.dateCreated) />
        <cfset setAdminID(arguments.adminID) />
        <cfreturn this />
    </cffunction>

</cfcomponent>
