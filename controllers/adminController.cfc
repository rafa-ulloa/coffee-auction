<cfcomponent accessors="1">
    
    <!--- properties //////////////////////////////////--->
    <cfproperty  name="adminDAO" type="adminDAO">

    <!--- public methods //////////////////////////////--->

    <!--- function: init --------------------------------->
    <cffunction  name="init" returntype="adminController" description="initializes component">
        <cfargument  name="adminDAO" type="AdminDAO" required="1">
        <cfset setAdminDAO(arguments.adminDAO) />
        <cfreturn this />
    </cffunction>

    <!--- function: searchAdmins ----------------------------->
    <cffunction name="searchAdmins" returntype="struct" description="get admin list. can be filtered down with arguments.">
        <cfargument type="numeric" required="0" name="id" default="0" />
        <cfargument type="string" required="0" name="username" default="" />
        <cfargument type="string" required="0" name="email" default="" />
        <cfargument type="numeric" name="blockFactor" hint="max records in data block (query) retrieved" />
        <cfargument type="numeric" name="blockOffset" hint="amount of data blocks to offset using the same block factor (for pagination)" />
        <cfset var arrAdmins = [] />
        <cfset var qAdmins = queryNew("") />
        <cfset var recordCount = 0 />
        <cfset var oQueryService = new services.queryService() />
        <cfset var totalPages = 0 />

        <!--- get total count of admins in the db that fit the criteria --->
        <cfset recordCount = getAdminDAO().getRecordCount(argumentCollection = arguments)  />
        <cfset totalPages = ceiling(recordCount/blockFactor) />

        <!--- get admins --->
        <cfset qAdmins = getAdminDAO().searchAdmins(argumentCollection = arguments) />

        <!--- convert query into objects --->
        <cfset arrAdmins = oQueryService.map(qAdmins,"admin") />

        <!--- return all necessary info to the view --->
        <cfreturn {
            recordCount = recordCount,
            totalPages = totalPages,
            currentPage = totalPages ? arguments.blockOffset + 1 : 0,
            results = arrAdmins
        } />

    </cffunction>

    <!--- function: updateAdmin --------------------------->
    <cffunction  name="updateAdmin" returntype="void" description="updates admin info or creates a new admin if ID = 0">
        <cfargument name="id" type="numeric" required="1" />
        <cfargument name="username" type="string" required="1" />
        <cfargument name="password" type="string" required="1" />
        <cfargument name="confirmPassword" type="string" required="1" />
        <cfargument name="email" type="string" required="1" />

        <!--- init vars --->
        <cfset var oQueryService = new services.queryService() />

        <!--- validate required fields --->
        <cfif
            !len(arguments.username) ||
            (!arguments.adminID && !len(arguments.password)) ||
            (!arguments.adminID && !len(arguments.confirmPassword)) ||
            !len(arguments.email)||
            !arguments.id>

            <cfthrow
                type="application"
                message="one or more of the required fields is missing"
                detail="make sure that the required fields are not empty" />

        </cfif>

        <!--- validate that passwords match --->
        <cfif (!arguments.id) && (arguments.password neq arguments.confirmPassword)>

            <cfthrow
                    type="application"
                    message="the password fields do not match"
                    detail="make sure the passwords match" />

        </cfif>

        <!--- validate that adminname is not already taken --->
        <cfif !arguments.adminID && getAdminDAO().searchAdmins(username = arguments.username).recordCount>

            <cfthrow type="application"
                    message="that username is already taken"
                    detail="please pick another adminname since this one is already taken" />

        </cfif>

        <!--- validate that email is not already taken --->
        <cfif !arguments.adminID && getAdminDAO().searchAdmins(email = arguments.email).recordCount>

            <cfthrow type="application"
                    message="that email is already taken"
                    detail="please pick another email since this one is already taken" />

        </cfif>

        <!--- validation passed, update/insert row --->
        <cfset getAdminDAO().updateAdmin(argumentCollection = arguments) />

    </cffunction>

    <!--- function: viewAdmin---------------------------->
    <cffunction name="viewAdmin" returntype="struct" description="gets info for admin detail page">
        <cfargument name="id" type="numeric" required="1" />
        <cfset var arrAdmin = [] />
        <cfset var qAdmin = getAdminDAO().searchAdmins(adminID = arguments.id) />
        <cfset var oQueryService = new services.queryService() />

        <!--- convert query into objects --->
        <cfset arrAdmin = oQueryService.map(qAdmin,"admin") />

        <cfreturn {
            admin = arguments.id ? arrAdmin[1] : new com.admins.Admin(0,"","","","","",0,now(),1)
        } />
    </cffunction>

    <!--- function: loginAdmin---------------------------->
    <cffunction name="loginAdmin" returntype="void" description="logs an admin in">
        <cfargument type="string" required="1" name="username" />
        <cfargument type="string" required="1" name="password" />
        <cfset var qAdmin = queryNew("") />
        <cfset var oAdmin = new com.users.Admin(0,"","",now()) />
        <cfset var oQueryService = new services.queryService() />

        <!--- get admin by login credentials --->
        <cfset qAdmin = getAdminDAO().getAdminByLogin(argumentCollection = arguments) />

        <!--- if the admin was found... --->
        <cfif qAdmin.recordCount>

            	<!--- map admin record to object --->
            	<cfset oAdmin = oQueryService.map(qAdmin,"admin")[1] />

		<!--- set client variables --->
		<cfset client.admin = serializeJSON(oAdmin) />

	<!--- if the admin was NOT found... --->
    	<cfelse>

		<!--- throw an error --->
	    	<cfthrow
			type="application"
		    	message="wrong username/password"
		    	detail="make sure you are typing in the correct username and password." />

	</cfif>
    </cffunction>

    <!--- function: reloadSessionAdmin--------------------->
    <cffunction name="reloadSessionAdmin" returntype="void" description="used when admin makes changes to account. rewrites the request.admin variable with the new updated info">
        <cfargument name="id" type="numeric" required="1" />
        <cfset var oAdmin = viewAdmin(arguments.id).admin />
        <cfset client.admin = serializeJSON(oAdmin) />
    </cffunction>

    <!--- function: changePassword ----------------------->
    <cffunction  name="changePassword" returntype="void" description="changes admin password">
        <cfargument name="id" type="numeric" required="1" />
        <cfargument name="password" type="string" required="1" />
        <cfargument name="confirmPassword" type="string" required="1" />


        <!--- validate required fields --->
        <cfif !arguments.id || !len(arguments.password)>

            <cfthrow
                    type="application"
                    message="one or more of the required fields is missing"
                    detail="make sure that the required fields are not empty" />

        </cfif>

        <!--- validate that passwords match --->
        <cfif arguments.password neq arguments.confirmPassword>

            <cfthrow
                    type="application"
                    message="the password fields do not match"
                    detail="make sure the passwords match" />

        </cfif>

        <!--- validation passed, update/insert row --->
        <cfset getAdminDAO().changePassword(argumentCollection = arguments) />

    </cffunction>

</cfcomponent>
