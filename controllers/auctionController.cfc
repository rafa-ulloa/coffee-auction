<cfcomponent accessors="1">
    
    <!--- properties //////////////////////////////////--->
    <cfproperty name="auctionDAO" type="AuctionDAO" />
    <cfproperty name="merchantDAO" type="MerchantDAO" />
    <cfproperty name="userDAO" type="UserDAO" />
    <cfproperty name="BidDAO" type="BidDAO" />
    <cfproperty name="bidRaiseMin" type="numeric" />

    <!--- public methods //////////////////////////////--->

    <!--- function: init --------------------------------->
    <cffunction  name="init" returntype="auctionController" description="initializes component">
        <cfargument  name="auctionDAO" type="AuctionDAO" required="1">
        <cfargument  name="merchantDAO" type="MerchantDAO" required="1">
        <cfargument  name="userDAO" type="UserDAO" required="1">
        <cfargument  name="bidDAO" type="BidDAO" required="1">
        <cfargument  name="bidRaiseMin" type="numeric" required="1">
        <cfset setAuctionDAO(arguments.auctionDAO) />
        <cfset setMerchantDAO(arguments.merchantDAO) />
        <cfset setUserDAO(arguments.userDAO) />
        <cfset setBidDAO(arguments.bidDAO) />
        <cfset setBidRaiseMin(arguments.bidRaiseMin) />
        <cfreturn this />
    </cffunction>

    <!--- function: searchAuctions ----------------------->
    <cffunction name="searchAuctions" returntype="struct" description="get auciton list. can be filtered down with arguments.">
        <cfargument name="id" type="numeric" required="0" />
        <cfargument name="startDate" type="date" required="0" />
        <cfargument name="endDate" type="date" required="0" />
        <cfargument name="merchantID" type="numeric" required="0" />
        <cfargument name="merchantName" type="string" required="0" />
        <cfargument name="dateCreated" type="date" required="0" />
        <cfargument name="adminID" type="numeric" required="0" />
        <cfargument type="numeric" name="blockFactor" hint="max records in data block (query) retrieved" />
        <cfargument type="numeric" name="blockOffset" hint="amount of data blocks to offset using the same block factor (for pagination)" />
        <cfset var arrAuctions = [] />
        <cfset var arrMerchants = [] />
        <cfset var qAuctions = queryNew("") />
        <cfset var qMerchants = queryNew("") />
        <cfset var recordCount = 0 />
        <cfset var oQueryService = new services.queryService() />
        <cfset var totalPages = 0 />

        <!--- get total count of users in the db that fit the criteria --->
        <cfset recordCount = getAuctionDAO().getRecordCount(argumentCollection = arguments)  />
        <cfset totalPages = ceiling(recordCount/arguments.blockFactor) />

        <!--- get auctions and merchants --->
        <cfset qAuctions = getAuctionDAO().searchAuctions(argumentCollection = arguments) />
        <cfset qMerchants = getMerchantDAO().searchMerchants() />

        <!--- convert query into objects --->
        <cfset arrAuctions = oQueryService.map(qAuctions,"auction") />
        <cfset arrMerchants = oQueryService.map(qMerchants,"merchant") />

        <!--- return all necessary info to the view --->
        <cfreturn {
            recordCount = recordCount,
            totalPages = totalPages,
            currentPage = totalPages ? arguments.blockOffset + 1 : 0,
            results = arrAuctions,
            merchants = arrMerchants
        } />

    </cffunction>

    <!--- function: updateAuction ------------------------>
    <cffunction  name="updateAuction" returntype="void" description="updates user info or creates a new user if ID = 0">
        <cfargument name="id" type="numeric" required="1" />
        <cfargument name="adminID" type="numeric" required="1" hint="for auditing purposes, admin who created user" />
        <cfargument name="startDate" type="date" required="1" />
        <cfargument name="endDate" type="date" required="1" />
        <cfargument name="merchantID" type="numeric" required="1" />
        <cfargument name="description" type="string" required="1" />
        <cfargument name="startingPrice" type="numeric" required="1" />

        <!--- validate required fields --->
        <cfif
            !arguments.adminID ||
            !len(arguments.startDate) ||
            !len(arguments.endDate) ||
            !arguments.merchantID ||
            !len(arguments.description) ||
	    !arguments.startingPrice>

            <cfthrow
                type="application"
                message="one or more of the required fields is missing"
                detail="make sure that the required fields are not empty" />

        </cfif>

        <!--- make sure auction end date is not before start date --->
        <cfif dateCompare(arguments.startDate, arguments.endDate) gt 0>
            <cfthrow
                type="application"
                message="auction start date cannot be later than auction end date."
                detail="make sure you have selected the right dates." />
        </cfif>

        <!--- validation passed, update/insert row --->
        <cfset getAuctionDAO().updateAuction(argumentCollection = arguments) />

    </cffunction>

    <!--- function: viewAuction--------------------------->
    <cffunction name="viewAuction" returntype="struct" description="gets info for auction detail page">
        <cfargument name="id" type="numeric" required="1" />
        <cfset var arrAuction = [] />
        <cfset var arrMerchants = [] />
        <cfset var arrHighestBid = [] />
        <cfset var qAuction = getAuctionDAO().searchAuctions(id = arguments.id) />
        <cfset var qHighestBid = getAuctionDAO().getAuctionHighestBid(id = arguments.id) />
        <cfset var qMerchants = getMerchantDAO().searchMerchants() />
	<cfset var qHighestBidder = qHighestBid.recordCount ? getUserDAO().searchUsers(userID = qHighestBid.userID) : queryNew("") />
        <cfset var oQueryService = new services.queryService() />

        <!--- convert query into objects --->
        <cfset arrAuction = oQueryService.map(qAuction, "auction") />
        <cfset arrMerchants = oQueryService.map(qMerchants, "merchant") />
        <cfset arrHighestBid = oQueryService.map(qHighestBid, "bid") />
	<cfset arrHighestBidder = oQueryService.map(qHighestBidder, "user") />

        <cfreturn {
            merchants = arrMerchants,
            auction = arguments.id ? arrAuction[1] : new com.auctions.Auction(),
            highestBid = arraylen(arrHighestBid) ? arrHighestBid[1] : new com.auctions.Bid(auctionID = arguments.id),
	    highestBidder = arrayLen(arrHighestBidder) ? arrHighestBidder[1] : new com.users.User(),
            bidRaiseMin = getBidRaiseMin()
        } />
    </cffunction>

    <!--- function: placeBidOnAuction -------------------->
    <cffunction name="placeBidOnAuction" returntype="void" description="places a bid on an auction">
    	<cfargument name="id" type="numeric" required="0" default="0" />
    	<cfargument name="auctionID" type="numeric" required="1" />
    	<cfargument name="userID" type="numeric" required="1" />
    	<cfargument name="amount" type="numeric" required="1" />

	<!--- validations --->
	<cfif
		!auctionID ||
		!userID ||
		!amount>
		<cfthrow
			type="application"
			message="One of the required fields is empty"
			detail="make sure all of the required values are being sent" />
	</cfif>
	
	<!--- place bid --->
	<cfset getBidDAO().updateBid(argumentCollection = arguments) />
    </cffunction>

    	<!---function:viewMyAuctions------------------------->
	<cffunction name="viewMyAuctions" returntype="struct" description="gets all my participating auctions.">
		<cfargument name="userID" type="numeric" required="1" />
		<cfset var qAuctionsParticipated = queryNew("") />
		<cfset var qAuctionsParticipatedStillActive = queryNew("") />
		<cfset var qAuctionsWon = queryNew("") />
		<cfset var qAuctionsWonPaid = queryNew("") />
		<cfset var qAuctionsWonUnpaid = queryNew("") />
		<cfset var qWinners = queryNew("") />
		<cfset var oQueryService = new services.queryService() />
		<cfset var arrAuctionsParticipatedStillActive = [] />
		<cfset var arrAuctionsWonPaid = [] />
		<cfset var arrAuctionsWonUnpaid = [] />
		
		<!--- get all auctions i have participated on --->
		<cfset qAuctionsParticipated = getAuctionDAO().getAuctionsByUserID(arguments.userID) />

		<!--- filter down to ones that are still active --->
		<cfquery name="qAuctionsParticipatedStillActive" dbtype="query">
			SELECT * FROM qAuctionsParticipated WHERE endDate >= #createDate(year(now()), month(now()), day(now()))#
		</cfquery>

		<!--- get the winner of every auction i have participated on,
		this will then be used to filter the ones won by me--->
		<cfset qWinners = getAuctionDAO().getAuctionWinners(valueList(qAuctionsParticipated.id)) />

		<!--- auctions i have won --->
		<cfquery name="qAuctionsWon" dbtype="query">
			SELECT auctionID FROM qWinners WHERE userID = #arguments.userID#
		</cfquery>

		<!--- filter down to auctions won that are paid --->
		<cfquery name="qAuctionsWonPaid" dbtype="query">
			SELECT * FROM qAuctionsParticipated WHERE id IN (#valueList(qAuctionsWon.auctionID)#) and isPaid = 1
		</cfquery>

		<!--- filter down to auctions won that are unpaid --->
		<cfquery name="qAuctionsWonUnpaid" dbtype="query">
			SELECT * FROM qAuctionsParticipated WHERE id IN (#valueList(qAuctionsWon.auctionID)#) and isPaid = 0 and endDate < #createDate(year(now()), month(now()), day(now()))#
		</cfquery>

		<!--- map auctions to objects --->
		<cfset arrAuctionsParticipatedStillActive = oQueryService.map(qAuctionsParticipatedStillActive, "auction") />
		<cfset arrAuctionsWonPaid = oQueryService.map(qAuctionsWonPaid, "auction") />
		<cfset arrAuctionsWonUnpaid = oQueryService.map(qAuctionsWonUnPaid, "auction") />

		<cfreturn {
			auctionsParticipatedStillActive = arrAuctionsParticipatedStillActive,
			auctionsWonPaid = arrAuctionsWonPaid,
			auctionsWonUnpaid = arrAuctionsWonUnpaid,
		} />

	</cffunction>

	<!---function:notifyAuctionWinners------------------------>
	<cffunction name="notifyAuctionWinners" returntype="void" description="notifies winners when auctions close">
		<cfargument name="endDate" type="date" required="0" default="#dateAdd("d", -1, now())#" hint="defaults to yesterday" />

		<!--- init vars --->
		<cfset var qAuctions = queryNew("") />
		<cfset var qWinners = queryNew("") />

		<!--- get auctions --->
		<cfset qAuctions = getAuctionDAO().getAuctionsWithEndDate(arguments.endDate) />

		<!--- if there are any auctions that closed the previous day... --->
		<cfif qAuctions.recordCount>

			<!--- get the winners for the auctions... --->
			<cfset qWinners = getAuctionDAO().getAuctionWinners(valueList(qAuctions.id)) />

			<!--- and proceed to notify them --->
			<cfloop query="qWinners">
			<cfmail from="#application.mailAddress#" to="#qWinners.email#" subject="Coffee Auction Winner" type="html">
			<cfloop query="qAuctions">
			<cfif qAuctions.id eq qWinners.auctionID>
				<cfoutput>
				<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
				<html xmlns="http://www.w3.org/1999/xhtml">
					<head>
			    			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			      			<meta name="viewport" content="width=device-width"/>
				  		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
				    		<style>
					  	</style>
					</head>
					<body class="container">
						<div class="row">
							<div class="col-sm">
								<h2>#application.companyName#</h2>
								<h1>Congratulations!</h1>
								<h3>You are the winning bidder.</h3>
								<p>You have #application.paymentGracePeriod# days to make your payment for the auction before it is conceded to the next runner-up. You can see the details by going into your #application.companyName# account or you can just <a class="btn btn-primary" href="http://#cgi.http_host#/auctions/payment/?id=#qAuctions.id#">Click Here</a></p>
							</div>
						</div>
					</body>
				</html>
				</cfoutput>
			</cfif>
			</cfloop>
			</cfmail>
			</cfloop>

		</cfif>


	</cffunction>

	<!---function:setAuctionAsPaid--------------------------->
	<cffunction name="setAuctionAsPaid" returntype="void">
		<cfargument name="id" type="numeric" required="1" hint="auction ID" />
		<cfargument name="paymentMethod" type="string" required="1" hint="paypal,visa,etc." />
		<cfargument name="paymentConfirmationNumber" type="string" required="1" />
		<cfset var paymentMethodID = application.qPaymentMethods["id"][listFind(valueList(application.qPaymentMethods.name),arguments.paymentMethod)] />
		
		<cfset getAuctionDAO().setAuctionAsPaid(arguments.id, paymentMethodID, arguments.paymentConfirmationNumber) />
	</cffunction>

</cfcomponent>
