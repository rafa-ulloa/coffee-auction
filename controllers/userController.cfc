<cfcomponent accessors="1">
    
    <!--- properties //////////////////////////////////--->
    <cfproperty  name="userDAO" type="userDAO">
    <cfproperty  name="countryDAO" type="countryDAO">
    <cfproperty  name="statesDAO" type="statesDAO">

    <!--- public methods //////////////////////////////--->

    <!--- function: init --------------------------------->
    <cffunction  name="init" returntype="userController" description="initializes component">
        <cfargument  name="userDAO" type="UserDAO" required="1">
        <cfargument name="countryDAO" type="CountryDAO" required="1" />
        <cfargument name="statesDAO" type="StatesDAO" required="1" />
        <cfset setUserDAO(arguments.userDAO) />
        <cfset setCountryDAO(arguments.countryDAO) />
        <cfset setStatesDAO(arguments.statesDAO) />
        <cfreturn this />
    </cffunction>

    <!--- function: searchUsers ----------------------------->
    <cffunction name="searchUsers" returntype="struct" description="get user list. can be filtered down with arguments.">
        <cfargument type="numeric" required="0" name="userID" default="0" />
        <cfargument type="string" required="0" name="username" default="" />
        <cfargument type="string" required="0" name="email" default="" />
        <cfargument type="numeric" required="0" name="countryID" default="0" />
        <cfargument type="numeric" required="0" name="adminID" default="0" />
        <cfargument type="numeric" name="blockFactor" hint="max records in data block (query) retrieved" />
        <cfargument type="numeric" name="blockOffset" hint="amount of data blocks to offset using the same block factor (for pagination)" />
        <cfset var arrUsers = [] />
        <cfset var arrCountries = [] />
        <cfset var qUsers = queryNew("") />
        <cfset var qCountries = queryNew("") />
        <cfset var recordCount = 0 />
        <cfset var oQueryService = new services.queryService() />
        <cfset var totalPages = 0 />

        <!--- get total count of users in the db that fit the criteria --->
        <cfset recordCount = getUserDAO().getRecordCount(argumentCollection = arguments)  />
        <cfset totalPages = ceiling(recordCount/blockFactor) />

        <!--- get users --->
        <cfset qUsers = getUserDAO().searchUsers(argumentCollection = arguments) />
        <cfset qCountries = getCountryDAO().getCountries() />

        <!--- convert query into objects --->
        <cfset arrUsers = oQueryService.map(qUsers,"user") />
        <cfset arrCountries = oQueryService.map(qCountries, "country") />

        <!--- return all necessary info to the view --->
        <cfreturn {
            countries = arrCountries,
            recordCount = recordCount,
            totalPages = totalPages,
            currentPage = totalPages ? arguments.blockOffset + 1 : 0,
            results = arrUsers
        } />

    </cffunction>

    <!--- function: updateUser --------------------------->
    <cffunction  name="updateUser" returntype="void" description="updates user info or creates a new user if ID = 0">
        <cfargument name="userID" type="numeric" required="1" />
        <cfargument name="username" type="string" required="1" />
        <cfargument name="password" type="string" required="1" />
        <cfargument name="confirmPassword" type="string" required="1" />
        <cfargument name="email" type="string" required="1" />
        <cfargument name="countryID" type="numeric" required="1" />
        <cfargument name="adminID" type="numeric" required="1" hint="for auditing purposes, admin who created user" />
        <cfargument name="shippingAddress_line1" type="string" required="1" />
        <cfargument name="shippingAddress_line2" type="string" required="0" default="" />
        <cfargument name="state" type="string" required="0" default="" />
        <cfargument name="otherState" type="string" required="0" default="" />
        <cfargument name="city" type="string" required="0" default="" />

        <!--- init vars --->
        <cfset var oQueryService = new services.queryService() />
        <cfset var qCountries = getCountryDAO().getCountries() />
        <cfset var arrCountries = oQueryService.map(qCountries, "country") />

        <!--- validate required fields --->
        <cfif
            !len(arguments.username) ||
            (!arguments.userID && !len(arguments.password)) ||
            (!arguments.userID && !len(arguments.confirmPassword)) ||
            !len(arguments.email)||
            !len(arguments.city)||
            !len(arguments.shippingAddress_line1)||
            !arguments.countryID ||
            !arguments.adminID>

            <cfthrow
                type="application"
                message="one or more of the required fields is missing"
                detail="make sure that the required fields are not empty" />

        </cfif>

        <!--- validate that the proper form field for "state" was filled for US/CA or otherwise --->
        <cfloop array="#arrCountries#" index="i" item="it">
            <cfif it.getID() eq arguments.countryID>
                <cfif listContains("US,CA", it.getISO())>
                    <cfif !len(arguments.state)>
                        <cfthrow
                                type="application"
                                message="you did not specify a state"
                                detail="make sure to select a state from the drop down" />
                    <cfelse>
                        <cfset arguments.state = getStatesDAO().getStateByID(arguments.state)["name"] />
                    </cfif>
                <cfelse>
                    <cfif !len(arguments.otherState)>
                        <cfthrow
                                type="application"
                                message="you did not specificy a state/province"
                                detail="make sure to specificy a state/province in the form" />
                    <cfelse>
                        <cfset arguments.state = arguments.otherState />
                    </cfif>
                </cfif>
            </cfif>
        </cfloop>

        <!--- validate that passwords match --->
        <cfif (!arguments.userID) && (arguments.password neq arguments.confirmPassword)>

            <cfthrow
                    type="application"
                    message="the password fields do not match"
                    detail="make sure the passwords match" />

        </cfif>

        <!--- validate that username is not already taken --->
        <cfif !arguments.userID && getUserDAO().searchUsers(username = arguments.username).recordCount>

            <cfthrow type="application"
                    message="that username is already taken"
                    detail="please pick another username since this one is already taken" />

        </cfif>

        <!--- validate that email is not already taken --->
        <cfif !arguments.userID && getUserDAO().searchUsers(email = arguments.email).recordCount>

            <cfthrow type="application"
                    message="that email is already taken"
                    detail="please pick another email since this one is already taken" />

        </cfif>

        <!--- validation passed, update/insert row --->
        <cfset getUserDAO().updateUser(argumentCollection = arguments) />

    </cffunction>

    <!--- function: viewUser---------------------------->
    <cffunction name="viewUser" returntype="struct" description="gets info for user detail page">
        <cfargument name="id" type="numeric" required="1" />
        <cfset var arrUser = [] />
        <cfset var arrCountries = [] />
        <cfset var qUser = getUserDAO().searchUsers(userID = arguments.id) />
        <cfset var qCountries = getCountryDAO().getCountries() />
        <cfset var qStates = getStatesDAO().getStates() />
        <cfset var oQueryService = new services.queryService() />

        <!--- convert query into objects --->
        <cfset arrCountries = oQueryService.map(qCountries, "country") />
        <cfset arrStates = oQueryService.map(qStates, "state") />
        <cfset arrUser = oQueryService.map(qUser,"user") />

        <cfreturn {
            countries = arrCountries,
            states = arrStates,
            user = arguments.id ? arrUser[1] : new com.users.User(0,"","","","","",0,now(),1)
        } />
    </cffunction>

    <!--- function: loginUser----------------------------->
    <cffunction name="loginUser" returntype="void" description="logs a user in">
        <cfargument type="string" required="1" name="username" />
        <cfargument type="string" required="1" name="password" />
        <cfset var qUser = queryNew("") />
        <cfset var oUser = new com.users.User() />
        <cfset var oQueryService = new services.queryService() />

        <!--- get user by login credentials --->
        <cfset qUser = getUserDAO().getUserByLogin(argumentCollection = arguments) />

        <!--- if the user was found... --->
        <cfif qUser.recordCount>

            <!--- map user record to object --->
            <cfset oUser = oQueryService.map(qUser,"user")[1] />

            <!--- set client variables --->
            <cfset client.user = serializeJSON(oUser) />

        <!--- if the user was NOT found... --->
        <cfelse>

            <!--- throw an error --->
            <cfthrow
                type="application"
                message="wrong username/password"
                detail="make sure you are typing in the correct username and password." />

        </cfif>
    </cffunction>

    <!--- function: loginAdmin---------------------------->
    <cffunction name="loginAdmin" returntype="void" description="logs an admin in">
        <cfargument type="string" required="1" name="username" />
        <cfargument type="string" required="1" name="password" />
        <cfset var qAdmin = queryNew("") />
        <cfset var oAdmin = new com.users.Admin(0,"","",now()) />
        <cfset var oQueryService = new services.queryService() />

        <!--- get user by login credentials --->
        <cfset qAdmin = getAdminDAO().getAdminByLogin(argumentCollection = arguments) />

        <!--- if the user was found... --->
        <cfif qUser.recordCount>

            <!--- map user record to object --->
            <cfset oUser = oQueryService.map(qUser,"user")[1] />

            <!--- set client variables --->
            <cfset client.user = serializeJSON(oUser) />

            <!--- if the user was NOT found... --->
            <cfelse>

            <!--- throw an error --->
            <cfthrow
                    type="application"
                    message="wrong username/password"
                    detail="make sure you are typing in the correct username and password." />

        </cfif>
    </cffunction>

    <!--- function: reloadSessionUser--------------------->
    <cffunction name="reloadSessionUser" returntype="void" description="used when user makes changes to account. rewrites the request.user variable with the new updated info">
        <cfargument name="id" type="numeric" required="1" />
        <cfset var oUser = viewUser(arguments.id).user />
        <cfset client.user = serializeJSON(oUser) />
    </cffunction>

    <!--- function: changePassword ----------------------->
    <cffunction  name="changePassword" returntype="void" description="changes user password">
        <cfargument name="userID" type="numeric" required="1" />
        <cfargument name="password" type="string" required="1" />
        <cfargument name="confirmPassword" type="string" required="1" />


        <!--- validate required fields --->
        <cfif !arguments.userID || !len(arguments.password)>

            <cfthrow
                    type="application"
                    message="one or more of the required fields is missing"
                    detail="make sure that the required fields are not empty" />

        </cfif>

        <!--- validate that passwords match --->
        <cfif arguments.password neq arguments.confirmPassword>

            <cfthrow
                    type="application"
                    message="the password fields do not match"
                    detail="make sure the passwords match" />

        </cfif>

        <!--- validation passed, update/insert row --->
        <cfset getUserDAO().changePassword(argumentCollection = arguments) />

    </cffunction>

</cfcomponent>
