<!--- start footer --->
<footer class="row text-right">
    <div class="col-sm">
        <p>&copy; <cfset writeOutput(year(now())) /> Virtual Coffee Auctions</p>
    </div>
</footer>
<!--- end footer --->

</div>
<!--- end content --->
</body>
</html>
