<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
    <title>Coffee Auction</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/app.css">
    <script src="/js/jquery-3.3.1.min.js"></script>
    <script src="/js/popper.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/jquery.countdown.min.js"></script>
    <script src="/js/admin.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
</head>
<body>

<!--- start navbar --->
<nav class="navbar navbar-expand-lg navbar-light bg-light">

    <!--- logo/company name --->
    <a class="navbar-brand" href="/">Coffee Auction</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <!--- nav links --->
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
        <cfif request.user.getID()>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Account
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
			<a class="dropdown-item" href="/account"><i class="fas fa-user"></i> Account Profile</a>
                    <a class="dropdown-item" href="/account/password"><i class="fas fa-key"></i> Change Password</a>
                    <a class="dropdown-item" href="/logout"><i class="fas fa-sign-out-alt"></i> Log Out</a>
                </div>
            </li>
        <cfelse>
            <li class="nav-item">
                <a class="btn btn-secondary" href="/login" role="button"><i class="fas fa-sign-in-alt"></i> Log In</a>
            </li>
        </cfif>
            <li class="nav-item">
                <a class="nav-link" href="/about">About</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/how">How it works</a>
            </li>
            <li class="nav-item">
                <a class="btn btn-primary" href="/auctions" role="button"><i class="fas fa-gavel"></i> Auctions</a>
            </li>
        </ul>
    </div>
</nav>
<!--- end navbar --->

<!--- start content --->
<div class="container-fluid">
