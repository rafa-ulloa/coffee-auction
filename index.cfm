<!--- page-specific styles --->
<cfsavecontent variable="tmpHTML">
<style type="text/css">
body{
    background: url("/img/coffee-beans-office-1024x576.jpg") bottom left no-repeat;
}
</style>
</cfsavecontent>
<cfhtmlhead text="#tmpHTML#" />

<!--- header --->
<header class="row">
    <div class="col-sm">
        <h1 class="display-1">Welcome</h1>
    </div>
</header>

<!--- start columns --->
<section class="row" id="home">

    <!--- left column --->
    <div class="col-sm text-center">
        <p class="lead">Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus.</p>
	<cfif !request.user.getID()>
		<a class="btn btn-primary" href="/sign-up"><i class="fas fa-user-plus"></i> Sign Up</a>
		<a class="btn btn-primary" href="/login"><i class="fas fa-sign-in-alt"></i> Log In</a>
	</cfif>
    </div>

</section>
