<!--- header --->
<header class="row">
    <div class="col-sm clearfix">
        <h1>Login</h1>
    </div>
</header>

<!--- start login content --->
<section class="row justify-content-center">
<div class="col-sm-12 col-md-6">

    <!--- start form --->
    <form id="form_login">

        <!--- username --->
        <div class="form-group row">
            <label for="form_login-input_username" class="col-sm-3 col-form-label">Username</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="form_login-input_username" name="username" placeholder="Username">
            </div>
        </div>

        <!--- password --->
        <div class="form-group row">
            <label for="form_login-input_password" class="col-sm-3 col-form-label">Password</label>
            <div class="col-sm-9">
                <input type="password" class="form-control" id="form_login-input_password" name="password" placeholder="password">
            </div>
        </div>

        <!--- submit --->
        <div class="form-group row">
            <div class="col-sm-12 text-center">
                <button type="submit" class="btn btn-primary" name="submit" value="submit"><i class="fas fa-sign-in-alt"></i> Log In</button>
            </div>
        </div>

    </form>
    <!--- end form --->

</div>
</section>

<!--- scripts --->
<script type="text/javascript">
//load on document ready//////////////////////////
$(function(){

    //general variables
    var loginForm = $("#form_login");
    var loginFormUsernameField = $("#form_login-input_username");
    var loginFormPasswordField = $("#form_login-input_password");

    //form submit
    loginForm.on("submit", function(e){

        //stop form from natural submit
        e.preventDefault();

        //make ajax request
        $
            .post(
                "/login/ajax/index.cfm",
                loginForm.serialize()
            )
            .done(function(){
                window.location = "/account"
            })
            .fail(function(response){
                alert(response.getResponseHeader("x-error-message"));
            });

    });

});
</script>