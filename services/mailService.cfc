<cfcomponent accessors="1">

	<!--- properties /////////////////////////////////////--->
	<cfproperty name="sender" type="string" />
	
	<!---  public functions //////////////////////////////--->
	<cffunction name="init" returntype="mailService">
		<cfargument name="sender" type="string" required="1" /> 
		<cfset setSender(arguments.sender) />
		<cfreturn this />
	</cffunction>
	
	<cffunction name="sendMail" returntype="void" />
		<cfargument name="receiver" type="string" required="1" />
		<cfargument name="subject" type="string" required="1" />
		<cfargument name="body" type="emailBody" required="1" />
		<cfmail from="#getSender()#" to="#arguments.receiver#" type="html">
			<cfset writeOutput(arguments.body.getHTMLBody()) />
		</cfmail>
	</cffunction>

</cfcomponent>
