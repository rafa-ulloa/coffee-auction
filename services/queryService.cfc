<cfcomponent accessors="1">

    <!--- properties //////////////////////////////////--->
    <cfproperty name="arrORM" type="array" />

    <!--- public functions ////////////////////////////--->

    <!--- function: init --------------------------------->
    <cffunction name="init" returntype="queryService">
        <cfset var jsonMappingFile = deserializeJSON(fileRead("orm.json")) />
        <cfset setArrORM(jsonMappingFile) />
        <cfreturn this />
    </cffunction>

    <!--- function: map ---------------------------------->
    <cffunction name="map" returntype="Array" description="convert database query into an array of objects">
        <cfargument type="query" name="query" required="1" hint="query to convert" />
        <cfargument type="string" name="class" required="1" hint="type of class to map each query records. can be 'user','auction', etc." />
        <cfset var arrResults = [] />
        <cfset var stMappings = {} />
        <cfset var stProperties = {} />

        <!--- extract class mapping from ORM lookup table --->
        <cfloop array="#getArrORM()#" index="i" item="it">
            <cfif arguments.class eq it.objectName>
                <cfset stMappings = it />
                <cfbreak />
            </cfif>
        </cfloop>

        <!--- throw error if there was no mapping for the class found --->
        <cfif !structCount(stMappings)>
            <cfthrow type="application" message="no ORM found for #arguments.class# class"
                    detail="make sure to add the mapping in the orm.json file and reload this service" />
        </cfif>

        <!--- loop through query and make the object conversion --->
        <cfloop query="arguments.query">

            <!--- clear the stProperties transfer object to populate with the values for the new row --->
            <cfset stProperties = {} />

            <!--- loop through the columns and copy the values onto a struct with the object properties matching the column --->
            <cfloop array="#stMappings.mappings#" item="it">
                <cfset stProperties[it.objectPropertyName] = arguments.query[it.queryColumnName] />
            </cfloop>

            <!--- create the object and add it to the results array --->
            <cfset arrayAppend(arrResults, new "#stMappings.class#"(argumentCollection = stProperties)) />
        </cfloop>

        <!--- return results --->
        <cfreturn arrResults />
    </cffunction>

</cfcomponent>
