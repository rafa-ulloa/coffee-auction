<!--- get data --->
<cfparam type="numeric" name="url.id" default="0" />
<cfset stData = application.userController.viewUser(url.id) />

<!--- header --->
<header class="row">
<div class="col-sm">
  <h1>Sign Up</h1>
</div>
</header>

<!--- start signup section --->
<section class="row justify-content-center">
  <div class="col-sm-12 col-md-6">

    <!--- start form --->
    <form id="form_signup">

      <!--- hidden fields --->
      <input type="hidden" name="userID" value="0">
      <input type="hidden" name="adminID" value="1">

      <!--- username --->
      <div class="form-group row">
        <label for="form_signup-input_username" class="col-sm-3 col-form-label">Username</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="form_signup-input_username" name="username" placeholder="Username">
        </div>
      </div>

      <!--- email --->
      <div class="form-group row">
        <label for="form_signup-input_email" class="col-sm-3 col-form-label">Email</label>
        <div class="col-sm-9">
          <input type="email" class="form-control" id="form_signup-input_email" name="email" placeholder="Email">
        </div>
      </div>

      <!--- country --->
      <div class="form-group row">
        <label for="form_signup-input_country" class="col-sm-3 col-form-label">Country</label>
        <div class="col-sm-9">
          <select class="form-control" id="form_signup-input_country" name="countryID">
              <option value="0">Select one</option>
          </select>
        </div>
      </div>

      <!--- state --->
      <div class="form-group row">
          <label for="form_signup-input_state" class="col-sm-3 col-form-label">State</label>
          <div class="col-sm-9">
              <input type="text" class="form-control form-state-field" id="form_signup-input_otherState" name="otherState" placeholder="State/Province" />
              <select class="form-control form-state-field" id="form_signup-input_state" name="state"></select>
          </div>
      </div>

      <!--- city --->
      <div class="form-group row">
          <label for="form_signup-input_city" class="col-sm-3 col-form-label">City</label>
          <div class="col-sm-9">
              <input type="text" class="form-control form-city-field" id="form_signup-input_city" name="city" placeholder="City" />
          </div>
      </div>

      <!--- address line 1 --->
      <div class="form-group row">
          <label for="form_signup-input_shippingAddress_line1" class="col-sm-3 col-form-label">Address Line 1</label>
          <div class="col-sm-9">
              <input type="text" class="form-control" id="form_signup-input_shippingAddress_line1" name="shippingAddress_line1" placeholder="Street Address" />
          </div>
      </div>

      <!--- address line 2 --->
      <div class="form-group row">
          <label for="form_signup-input_shippingAddress_line2" class="col-sm-3 col-form-label">Address Line 2</label>
          <div class="col-sm-9">
              <input type="text" class="form-control" id="form_signup-input_shippingAddress_line2" name="shippingAddress_line2" placeholder="Apartment or Suite Number (optional)" />
          </div>
      </div>

      <!--- password --->
      <div class="form-group row">
        <label for="form_signup-input_password" class="col-sm-3 col-form-label">Password</label>
        <div class="col-sm-9">
          <input type="password" class="form-control" id="form_signup-input_password" name="password" placeholder="Password" />
        </div>
      </div>
      
      <!--- confirm password --->
      <div class="form-group row">
        <label for="form_signup-input_password-confirm" class="col-sm-3 col-form-label">Confirm Password</label>
        <div class="col-sm-9">
          <input type="password" class="form-control" id="form_signup-input_password-confirm" name="confirmPassword" placeholder="Confirm password" />
        </div>
      </div>
      
      <!--- submit --->
      <div class="form-group row">
        <div class="col-sm-12 text-center">
          <button type="submit" class="btn btn-primary" name="submit" value="submit"><i class="fas fa-check"></i> Submit</button>
          <a class="btn btn-secondary" href="/"><i class="fas fa-times"></i> Cancel</a>
        </div>
      </div>

    </form>
<!--- end form --->

  </div>
</section>

<!--- scripts --->
<script type="text/javascript">
//load on document ready//////////////////////////
$(function(){

    //general variables---------------------------
    var arrCountries = <cfset writeOutput(serializeJSON(stData.countries)) />;
    var arrStates = <cfset writeOutput(serializeJSON(stData.states)) />;
    var USCountryID, CACountryID;

    //get US and CA country IDs
    $.each(arrCountries, function(i,v) {
        if (v.iso === "US"){
            USCountryID = v.id;
        } else if (v.iso === "CA"){
            CACountryID = v.id;
        } else{
            return true;
        }
    });

    //form control selectors----------------------
    var userForm = $("#form_signup");
    var userFormStateField = $("#form_signup-input_state");
    var userFormOtherStateField = $("#form_signup-input_otherState");
    var userFormCountryField = $("#form_signup-input_country");

    //populate country select
    $.each(arrCountries, function(i,v){
        userFormCountryField.append('<option value="' + v.id + '">' + v.name + '</option>');
    });

    //switch state field depending on country-----
    userFormCountryField.on("change", function() {
      var countryID = parseInt($(this).val());

      //hide state fields
      userFormStateField.hide();
      userFormOtherStateField.hide();

      //first determine whether to show a dropdown or text
      // if not US/CA, show text
      if ([USCountryID,CACountryID].indexOf(countryID) < 0){
          userFormOtherStateField.show();

          //else, if it is US/CA
      } else {

          //empty the dropdown before populating with corresponding states
          userFormStateField.empty();

          //loop through the object containing US/CA states
          $.each(arrStates, function(i,v){

              //filter only states belonging to the currently selected country
              if (v.countryID === countryID){

                  //add each of the selected country's state to the dropdown
                  userFormStateField.append('<option value="' + v.id + '">' + v.name + '</option>');

              }

          });

          userFormStateField.show();

        }

    }).change() //trigger change on document load as well.

    //user form submit----------------------------
    userForm.on("submit", function(e){

        e.preventDefault();

        $.post(
          "/sign-up/ajax/index.cfm",
          userForm.serialize()
        )
        .done(function(data){
            document.location = "/sign-up/thank-you";
        })
        .fail(function(response){
            alert(response.getResponseHeader("x-error-message"));
        });

    });

});
</script>
