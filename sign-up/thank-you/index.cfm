<!--- header --->
<header class="row">
    <div class="col-sm">
        <h1 class="display-1">Your signup is complete. Welcome Aboard.</h1>
    </div>
</header>

<!--- start columns --->
<section class="row" id="home">

<!--- left column --->
    <div class="col-sm text-center">
        <p class="lead">You may now place bids on our coffee auction. All you have to do is log in to your account.</p>
        <a class="btn btn-primary" href="/login"><i class="fas fa-sign-in-alt"></i> Log In</a>
    </div>

</section>